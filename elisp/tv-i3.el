(declare-function i3-command "i3")

;; https://www.reddit.com/r/emacs/comments/5at7lg/vanquishing_emacs_displaybuffer_for_use_with_i3/d9jldgq/
(defun tv/i3-split-vertical ()
  (interactive)
  (i3-command 0 "split vertical")
  (new-frame))

(defun tv/i3-split-horizontal ()
  (interactive)
  (i3-command 0 "split horizontal")
  (new-frame))

(defun tv/i3-delete-window-or-frame ()
  (interactive)
  (condition-case _
      (delete-window)
    (error (delete-frame))))

;; ;; From https://www.emacswiki.org/emacs/frame-cmds.el
;; (defadvice delete-window (around delete-frame-if-one-win activate)
;;   "If WINDOW is the only one in its frame, then `delete-frame' too."
;;   (if (fboundp 'with-selected-window)   ; Emacs 22+
;;       (with-selected-window
;;           (or (ad-get-arg 0)  (selected-window))
;;         (if (one-window-p t) (delete-frame) ad-do-it))
;;     (save-current-buffer
;;       (select-window (or (ad-get-arg 0)  (selected-window)))
;;       (if (one-window-p t) (delete-frame) ad-do-it)))) 


(defun tv/i3--next-frame (window back?)
  (let* ((frames (visible-frame-list))
	 (n (length frames))
	 (i (seq-position frames (window-frame window)))
	 (nxt (mod (+ i (if back? -1 1)) n)))
    ;; (message (format "%s" frames))
    ;; (message (format "Frame: i=%s n=%s nxt=%s" i n nxt))
    (seq-elt frames nxt)))

(defun tv/i3--next-window (window back?)
  (let* ((frame (window-frame window))
	 (windows (window-list frame nil
			       (frame-first-window frame)))
	 (n (length windows))
	 (i (seq-position windows window))
	 (nxt (+ i (if back? -1 1))))
    ;; (message (format "%s" windows))
    ;; (message (format "Window: i=%s n=%s nxt=%s" i n nxt))
    (if (= nxt (if back? -1 n))
	nil
      (seq-elt windows nxt))))

(defun tv/i3--next-window-vis-frames (window back?)
  (or (tv/i3--next-window window back?)
      (frame-first-window (tv/i3--next-frame window back?))))

;; (defun tv/i3--next-window (window)
;;   (or (window-next-sibling window)
;;       (frame-first-window (tv/i3--frame-next-sibling window))))

;; (defun tv/i3--prev-window (window)
;;   (or (window-prev-sibling window)
;;       (frame-first-window (tv/i3--frame-next-sibling window t))))


(defun tv/i3--find-other-window (window count)
  (cond
   ((= count 0) window)
   ((> count 0)
    (dotimes (i count window)
      (setq window (tv/i3--next-window-vis-frames
		    window nil))))
   ((< count 0)
    (dotimes (i (- count) window)
      (setq window (tv/i3--next-window-vis-frames
		    window t))))))

(defun tv/i3-other-window (count)
  (interactive "p")
  (let ((window (tv/i3--find-other-window
		 (get-buffer-window) count)))
    ;; (message (format "%s %s" count window))
    (select-window window)
    (select-frame-set-input-focus (window-frame))))
  
