;; Compiler declarations

(declare-function TeX-active-mark "tex")
(declare-function TeX-command-region "tex-buf")
(declare-function LaTeX-command-section "tex-buf")
(declare-function TeX-error-overview "tex-buf")
(declare-function TeX-error-overview-quit "tex-buf")
(declare-function LaTeX-insert-environment "latex")
(declare-function LaTeX-mark-environment "latex")
(declare-function LaTeX-label "latex")
(declare-function LaTeX-newline "latex")
(declare-function LaTeX-env-beginning-pos-col "latex")
(declare-function LaTeX-indent-level-count "latex")

(declare-function reftex-locate-bibliography-files "reftex-parse.el")

(declare-function ivy-bibtex-with-local-bibliography "ivy-bibtex")
(declare-function ivy-bibtex "ivy-bibtex")

(declare-function cdlatex-sub-superscript "cdlatex")

(defvar TeX-command-buffer)
(defvar LaTeX-default-document-environment)
(defvar LaTeX-indent-environment-list)
(defvar bibtex-completion-bibliography)

(defvar tv/TeX-window-configuration)


;; Functions

(defun tv/latex-mark-named-environment (name)
  (interactive)
  (let ((latex-syntactic-comments nil)
	(fail-regexp "\\\\begin{document}")
	(found-regexp (format "\\\\begin{%s}" name))
	(break nil))
    (while (and (not break)
		(not (looking-at found-regexp)))
      (if (looking-at fail-regexp)
	  (progn
	    (message "No valid environment found")
	    (setq break t))
	(LaTeX-mark-environment)))))

;; In beamer, running a command on a frame is useful. I guess a better
;; way to do all of it would be to change
;; `LaTeX-command-section-boundaries' to handle that case too.

(defun tv/latex-command-block (&optional override-confirm)
  "Run compilation on current block

A block is a document-segmenting environment, such as frame in
beamer or letter in letter. If the current class has no defined
blocks, the compilation runs on the current section.
"
  (interactive "P")
  (if LaTeX-default-document-environment
      (save-mark-and-excursion
	(tv/latex-mark-named-environment LaTeX-default-document-environment)
	(TeX-command-region override-confirm))
    (LaTeX-command-section override-confirm)))


;; Sub or superscript

(defvar-local tv/cdlatex-sub-superscript-last-beg nil)
(defvar-local tv/cdlatex-sub-superscript-last-end nil)

(defun tv/cdlatex-sub-superscript (&optional beg end)
  "Insert the subscript or superscript, and handle the argument

If there is an active region, the character is inserted before the region and the region is wrapped in braces. Point is moved after the closing brace.

Otherwise, the character is inserted at point. If the next
characters are numbers, they are wrapped in braces and point is
moved after them. Otherwise, a pair of braces is inserted and
point is moved between them.
"
  (interactive
   ;; https://stackoverflow.com/a/13282015/1083706
   (if (use-region-p)
       (list (region-beginning) (region-end))
     (list nil nil)))
  (message "%s <- %s <- " this-command last-command)
  (if beg
      (let ((string (buffer-substring beg end)))
	;; We don't use kill and yank in order not to mess with the value of last-command
	(delete-region beg end)
	(goto-char beg)
	(cdlatex-sub-superscript)
	(setq tv/cdlatex-sub-superscript-last-beg (point))
	(insert string)
	(setq tv/cdlatex-sub-superscript-last-end (point))
	(unless (= beg end)
	  (forward-char 1)))
    (if (eq this-command last-command)
	(let ((beg tv/cdlatex-sub-superscript-last-beg)
	      (end tv/cdlatex-sub-superscript-last-end))
	  (tv/cdlatex-sub-superscript beg end))
      (save-match-data
	(and (looking-at "[0-9]*")
	     (tv/cdlatex-sub-superscript (match-beginning 0) (match-end 0)))))))



;; Automatic insertion of long parenthesis pairs

(defun tv/latex-insert-paren (open close)
  (if (use-region-p)
      (let ((beg (min (point) (mark)))
	    (end (max (point) (mark))))
	(save-excursion
	  (goto-char end)
	  (insert close)
	  (goto-char beg)
	  (insert open)))
    (progn
      (insert open)
      (save-excursion
	(insert close)))))

(defun tv/latex-insert-leftright-paren ()
  (interactive)
  (tv/latex-insert-paren "\\left(" "\\right)"))

(defun tv/latex-insert-leftright-square ()
  (interactive)
  (tv/latex-insert-paren "\\left[" "\\right]"))

(defun tv/latex-insert-leftright-brace ()
  (interactive)
  (tv/latex-insert-paren "\\left\\{" "\\right\\}"))

(defun tv/latex-insert-leftright-angle ()
  (interactive)
  (tv/latex-insert-paren "\\left<" "\\right>"))


;;; Temporary fixes, before suggesting for patch

;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=28382
(defun LaTeX-env-label (environment)
  "Insert ENVIRONMENT and prompt for label."
  (LaTeX-insert-environment environment)
  (if (TeX-active-mark)
      (exchange-point-and-mark))
  (indent-according-to-mode)
   (when (LaTeX-label environment 'environment)
     (LaTeX-newline)
     (indent-according-to-mode))
   (if (TeX-active-mark)
       (exchange-point-and-mark)))

;; Getting the tex-overview-error to quit properly after
;; closing. Basically I am looking for an (imperfect) behavior ala
;; `edebug' or `ediff', where the window configuration is restored to what
;; it was before the buffer popped. Intentional changes made in
;; between will also be reverted, but I figure that if there was an
;; easy way to keep track of them, `edebug' and co would do it.

(defun tv/TeX-error-overview ()
  (interactive)
  (setq-local tv/TeX-window-configuration (current-window-configuration))
  (TeX-error-overview))

(defun tv/TeX-error-overview-quit ()
  (interactive)
  ;; The window configuration is buffer-local in the tex buffer
  (with-current-buffer TeX-command-buffer
      (if (bound-and-true-p tv/TeX-window-configuration)
	  (progn
	    (set-window-configuration tv/TeX-window-configuration)
	    (setq-local tv/TeX-window-configuration nil))
    (TeX-error-overview-quit))))

;; Indentation of tabulars

(defvar tv/latex-indent-tabular-main 2)
(defvar tv/latex-indent-tabular-line 2)
(defvar tv/latex-indent-tabular-cell 0)

;; (defun tv/latex-indent-tabular ()
;;   (let* ((beg-pos (car (LaTeX-env-beginning-pos-col)))
;; 	 (beg-col
;; 	  (save-excursion
;; 	    (goto-char beg-pos)
;; 	    (beginning-of-line)
;; 	    (+ (current-indentation)
;; 	       (LaTeX-indent-level-count)
;; 	       (- LaTeX-indent-level)))) ; Have to remove one for the tabular
;; 	 (name (LaTeX-current-environment))
;; 	 ;; (end-pos (save-excursion
;; 	 ;; 	    (LaTeX-find-matching-end)
;; 	 ;; 	    (point)))
;; 	 (end-regexp (format "\\\\end{%s}" name))
;; 	 (beg-regexp (format "\\\\begin{%s}" name))
;; 	 ;; (cr-lf-regexp (format "\\(%s\\|\\\\\\\\\\|[^\\]&\\)" beg-regexp))
;; 	 (cr-lf-regexp  "\\\\\\\\\\|[^\\]&")
;; 	 )
;;     (cond ((looking-at end-regexp)
;; 	   beg-col)
;; 	  ;; ((looking-at "\\\\\\\\")
;; 	  ;;  (+ tv/latex-indent-tabular-line beg-col))
;; 	  ;; ((looking-at "&")
;; 	  ;;  (+ tv/latex-indent-tabular-line beg-col))
;; 	  ;;;; If I put a newline it's that the line was too long
;; 	  (t
;; 	   (+
;; 	    tv/latex-indent-tabular-main
;; 	    (let ((last-change
;; 		  (save-excursion
;; 		    (when (re-search-backward cr-lf-regexp beg-pos t)
;; 		      (point)))))
;; 	     (if last-change
;; 		 (if (= ?& (char-before (match-end 0)))
;; 		     ;; We are in a cut cell
;; 		     (+ tv/latex-indent-tabular-line
;; 			tv/latex-indent-tabular-cell
;; 			beg-pos)
;; 		   ;; Else we are in a new line and we need to decide
;; 		   ;; if it was cut or not
;; 		   ())
;; 	       beg-pos))))))

(defun LaTeX-indent-tabular ()
  "Return indent column for the current tabular-like line."
  (let* ((beg-pos (car (LaTeX-env-beginning-pos-col)))
	 (beg-col
	  (save-excursion
	    (goto-char beg-pos)
	    (beginning-of-line)
	    (+ (current-indentation)
	       (LaTeX-indent-level-count)
	       (- 2))))) ; Was hard coded elsewhere before me...
    (let ((tabular-like-end-regex
	   (format "\\\\end{%s}"
		   (regexp-opt
		    (let (out)
		      (mapc (lambda (x)
                              (when (eq (cadr x) 'LaTeX-indent-tabular)
                                (push (car x) out)))
                            LaTeX-indent-environment-list)
		      out)))))
      (cond ((looking-at tabular-like-end-regex)
	     beg-col)
	    ((looking-at "\\\\\\\\")
	     (+ 4 beg-col))
	    ((looking-at "&")
	     (+ 4 beg-col)) ;; If I put a newline it's that the line was too long
	    (t
	     (+ 2
		(if (and (save-excursion
			   (re-search-backward "\\\\\\\\\\|[^\\]&" beg-pos t))
			 (= ?& (char-before (match-end 0))))
		    ;; If a new line is inserted it probably means
		    ;; that the line was too long
		    (+ 2 beg-col) 
		  beg-col)))))))


;; With orgtbl
(defun tv/tex-next-error-or-hijacker-23 ()
  "Dummy function to overload `C-c \"`

Will call `TeX-next-error` if outside an orgtbl table, and behave normally in an orgtbl table"
  (interactive)
  (if (fboundp 'orgtbl-hijacker-command-23)
      (call-interactively 'orgtbl-hijacker-command-23)
    (call-interactively 'TeX-next-error)))


;; With ivy-bibtex
(defun tv/ivy-bibtex-insert-citation ()
  (interactive)
  (let* ((ivy-bibtex-default-action 'ivy-bibtex-insert-citation)
	 (local-bib (reftex-locate-bibliography-files "./"))
	 (bibtex-completion-bibliography (or local-bib
                                             bibtex-completion-bibliography)))
    (ivy-bibtex nil local-bib)))


(provide 'tv-latex)
