(declare-function pdf-view-next-line-or-next-page "pdf-view")
(declare-function pdf-view-previous-line-or-previous-page "pdf-view")
(defvar tv/small-screen)

;; Defuns go here for byte-compiling

(defun tv/find-file (file &optional new-frame)
  (let ((ff-fun (if new-frame 'find-file-other-frame 'find-file)))
    (set-buffer (funcall ff-fun file))))

(defun tv/back-to-indentation-or-to-bol ()
  "Move back to indentation, or if we already were, to the beginning of the line."
  (interactive)
  (let ((cur-point (point)))
    (if visual-line-mode
        (progn
          (beginning-of-visual-line 1)
          (let* ((bol (point))
                 (bti (save-excursion
                        (back-to-indentation)
                        (point))))
            (when (> bti bol) (goto-char bti))))
      (back-to-indentation)) ;; FIXME?
    (when (eq cur-point (point)) (beginning-of-visual-line 1))))

(defun tv/end-of-syntax-or-line ()
  "Move to last non-space, non-comment point, or, if we already were, to the end of the line."
  (interactive)
  ;; TODO
  )


;; Source: https://stackoverflow.com/a/3024055/1083706
(defun tv/stop-using-minibuffer ()
  "kill the minibuffer"
  (when (and (>= (recursion-depth) 1) (active-minibuffer-window))
    (abort-recursive-edit)))

(defun tv/kill-or-join-line (&optional arg)
  "Kill the line or join it with the following

Kill the remaining part of the line or join this line and the
  following one if looking at the end of line."
  (interactive "P")
  (if (looking-at "[[:blank:]]*$")
      (join-line t)
    (if visual-line-mode
	(kill-visual-line arg)
      (kill-line arg))))

(defun tv/warn-about-C-z ()
  "Dummy function to be bound to `C-z'"
  (interactive)
  (message (concat
	    "C-z is disabled.\n\n"
	    "Minimize window / suspend : C-M-z\n" 
	    "Beginning of line         : C-a\n"
	    "End of line               : C-e\n"
	    "Undo                      : C-_ or C-S-z")))

(defun tv/copy-whole-buffer ()
  (interactive)
  (kill-ring-save (point-min) (point-max)))

(defun tv/add-declare-function (fun)
  (interactive "a")
  (let* ((buf (car (find-function-noselect fun)))
	 (name (file-name-base (buffer-file-name buf))))
    (insert (format "(declare-function %s \"%s\")\n" fun name))))

;; (defun tv/other-window-or-frame (count)
;;   (interactive "P")
;;   (let ((movecnt
;; 	 (cond
;; 	  ((not count) 1)
;; 	  ((listp count) -1)
;; 	  (t count))))
;;     ;; (message (format "%s -> %s" count movecnt))
;;     (other-window movecnt 'visible)
;;     (message (format "%s" (current-buffer)))
;;     ;; I don't know why the next line is necessary but it is
;;     (select-frame-set-input-focus (window-frame))))

(defun tv/eval-other-window (form)
  (interactive)
  (let ((winconf (current-window-configuration))
	(inhibit-quit t))
    (switch-to-buffer-other-window (buffer-name))
    (with-local-quit
      (eval form))
    ;; If the user quit the form, restore the window setup
    (when quit-flag
      (set-window-configuration winconf))))
  
(defun tv/M-x-other-window ()
  (interactive)
  (tv/eval-other-window '(funcall (key-binding (kbd "M-x")))))

(defun tv/ielm-other-window ()
  (interactive)
  (tv/eval-other-window '(ielm)))

(defun tv/backspace-function (arg &optional killp)
  (interactive "*p\nP")
  (funcall (key-binding (kbd "DEL"))
	   (or arg 1) killp))

;; Insertion functions

(defun tv/insert-coloneq ()
  (interactive)
  (if (looking-back ":" (- (point) 1))
      (insert "=")
    (insert ":=")))


;; Scroll other window also for pdf-view
;; https://github.com/politza/pdf-tools/issues/55#issuecomment-230108362
(defun tv/scroll-other-window ()
  (interactive)
  (let* ((wind (other-window-for-scrolling))
         (mode (with-selected-window wind major-mode)))
    (if (eq mode 'pdf-view-mode)
        (with-selected-window wind
      (pdf-view-next-line-or-next-page 2))
      (scroll-other-window 2))))

(defun tv/scroll-other-window-down ()
  (interactive)
  (let* ((wind (other-window-for-scrolling))
         (mode (with-selected-window wind major-mode)))
    (if (eq mode 'pdf-view-mode)
    (with-selected-window wind
      (progn
        (pdf-view-previous-line-or-previous-page 2)
        (other-window 1)))
      (scroll-other-window-down 2))))

(defvar tv/text-left-margin 3)

(defun tv/get-left-margin ()
  (if tv/small-screen
      0 tv/text-left-margin))

(defun tv/vfc-set-widths (&optional window)
  "Ensure a minimal left margin for buffers with visual-fill-column mode"
  (let* ((window (or window (get-buffer-window (current-buffer))))
	 (left (or (car (window-margins window)) 0))
	 (right (or (cdr (window-margins window)) 0)))
    (set-window-margins window
			(+ left (tv/get-left-margin))
			(max 0 (- right (tv/get-left-margin))))))

;; Better backspace?
;; Does not work as expected: emacs groups undos, and e.g. C-c C-j in latex is not a self-insert-command
(defun tv/backspace-function (N &optional killflag)
  (interactive "p")
  (if (or killflag (> N 1)
	  (not (eq last-command 'self-insert-command)))
      (delete-backward-char N killflag)
    (undo)))


(provide 'tv-defuns)
