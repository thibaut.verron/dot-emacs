(defconst tv/build-directory (concat "build" "-" tv/system-string))

(defun straight--build-dir (&rest segments)
  "Get a subdirectory of the straight/build/ directory.
SEGMENTS are passed to `straight--dir'. With no SEGMENTS, return
the straight/build/ directory itself."
  (apply #'straight--dir tv/build-directory segments))

(defun straight--build-file (&rest segments)
  "Get a file in the straight/build/ directory.
SEGMENTS are passed to `straight--file'."
  (apply #'straight--file tv/build-directory segments))

(defun straight--build-cache-file ()
  "Get the file containing straight.el's build cache."
  (straight--file (concat tv/build-directory "-cache.el")))

(defun straight--compute-dependencies (package)
  "Register the dependencies of PACKAGE in `straight--build-cache'.
PACKAGE should be a string naming a package. Note that this
function does *not* return the dependency list; see
`straight--get-dependencies' for that. (The reason these two
functions are separate is because dependencies are computed at
package build time, but they are retrieved later (when we are
activating autoloads, and may not have even built the package on
this run of straight.el)."
  (let ((dependencies
         ;; There are actually two ways of specifying a package in
         ;; Emacs. The first is to include a file called
         ;; <PACKAGE-NAME>-pkg.el which contains a data structure with
         ;; a bunch of information (including the dependency alist).
         ;; The second is to put the information as headers in the
         ;; preamble of the file <PACKAGE-NAME>.el. We account for
         ;; both of them here.
         (or (ignore-errors
               (with-temp-buffer
                 ;; Bypass `find-file-hook'.
                 (insert-file-contents-literally
                  (straight--build-file
                    package
                   (format "%s-pkg.el" package)))
                 (straight--process-dependencies
                  (eval (nth 4 (read (current-buffer)))))))
             (ignore-errors
               (with-temp-buffer
                 (insert-file-contents-literally
                  (straight--build-file
                   package
                   (format "%s.el" package)))
                 ;; Who cares if the rest of the header is
                 ;; well-formed? Maybe package.el does, but all we
                 ;; really need is the dependency alist. If it's
                 ;; missing or malformed, we just assume the package
                 ;; has no dependencies.
                 (let ((case-fold-search t))
                   (re-search-forward "^;; Package-Requires: "))
                 (when (looking-at "(")
                   (straight--process-dependencies
                    (read (current-buffer)))))))))
    (straight--insert 1 package dependencies straight--build-cache)))
