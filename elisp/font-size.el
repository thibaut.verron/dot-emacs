;; -*- lexical-binding: t; -*-

;; Taken from: https://emacs.stackexchange.com/a/44930/184 and other answer

(require 'cl-lib)

(defun tv/dpi (&optional frame)
  "Get the DPI of FRAME (or current if nil)."
  (cl-flet ((pyth (lambda (w h)
                    (sqrt (+ (* w w)
                             (* h h)))))
            (mm2in (lambda (mm)
                     (/ mm 25.4))))
    (let* ((atts (frame-monitor-attributes frame))
           (pix-w (cl-fourth (assoc 'geometry atts)))
           (pix-h (cl-fifth (assoc 'geometry atts)))
           (pix-d (pyth pix-w pix-h))
           (mm-w (cl-second (assoc 'mm-size atts)))
           (mm-h (cl-third (assoc 'mm-size atts)))
           (mm-d (pyth mm-w mm-h)))
      (/ pix-d (mm2in mm-d)))))

;; Assoc list: monitor size - font spec
;; Font spec is an alist max dpi - font size
(defcustom tv/dpi-to-font-fixed-pitch
  '((1 . ((150 . 105)))
    (2 . ((100 . 102)
	  (150 . 145))))
  ""
  :group 'tv/custom)

(defcustom tv/dpi-to-font-variable-pitch
  '((1 . ((150 . 115)))
    (2 . ((100 . 115)
	  (150 . 160))))
  ""
  :group 'tv/custom)


(defun tv/adjust-font-size (&optional frame)
  (interactive (list (selected-frame)))
  (let ((dpi (tv/dpi frame))
	(nmons (length (display-monitor-attributes-list))))
    (message "Number of monitors: %s" nmons)
    (message "Frame is %s" frame)
    (message "Frame DPI is %s" dpi)
    (cl-loop
     for (bnd . size) in (cdr (cl-assoc nmons tv/dpi-to-font-fixed-pitch))
     while (and
	    (> dpi bnd)
	    (message "Setting default font to size %s" size)
	    (or (set-face-attribute 'default frame :height size) t)))
    (copy-face 'default 'fixed-pitch frame)
    (cl-loop
     for (bnd . size) in (cdr (cl-assoc nmons tv/dpi-to-font-variable-pitch))
     while (and
	    (> dpi bnd)
	    (message "Setting variable-pitch font to size %s" size)
	    (or (set-face-attribute 'variable-pitch frame :height size) t)))
  (unicode-fonts-setup)))

(define-minor-mode auto-font-size-mode
  ""
  :global t
  (if auto-font-size-mode
      (progn
	(add-hook 'after-make-frame-functions #'tv/adjust-font-size)
	(add-hook 'move-frame-functions #'tv/adjust-font-size))
    (progn
      (remove-hook 'after-make-frame-functions #'tv/adjust-font-size)
      (remove-hook 'move-frame-functions #'tv/adjust-font-size))))

(provide 'font-size)
