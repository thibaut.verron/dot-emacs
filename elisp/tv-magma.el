
(defun tv/magma-update-header-custom ()
  (save-excursion
    (goto-char (point-min))
    (when (looking-at "/[*/] -\\*-") (forward-line 1)) ;; prop-line
    (when (looking-at "// Created")
      (when (looking-at "// Created on")
        (replace-match "// Created:"))
      (forward-line 1)
      (when (looking-at "// Last modified:")
        (delete-region (point) (progn (forward-line 1) (point))))
      (insert (format "// Last modified: %s\n" (current-time-string)))
      (when (looking-at "// Hash:")
        (delete-region (point) (progn (forward-line 1) (point))))
      (let ((start
             (save-excursion
               (forward-line 2)
               (point))))
        (insert (format "// Hash: %s\n"
                        (secure-hash 'md5 (current-buffer)
                                     start
                                     (point-max))))))))

(defun tv/magma-insert-testeq ()
  (interactive)
  (if (magma-in-literal)
      (insert "=")
    (if (looking-back "[[:blank:]\n]" (- (point) 1))
        (insert "eq")
      (insert " eq "))))



(provide 'tv-magma)
