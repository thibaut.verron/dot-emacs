(declare-function ivy-state-caller "ivy")
(declare-function with-ivy-window "ivy")
(declare-function ivy-set-index "ivy")
(declare-function ivy-next-line "ivy")
(defvar ivy-last)
(defvar ivy--index)
(defvar ivy--length)
(defvar ivy--all-candidates)

;; (defun tv/-ivy-count-lines (pos1 pos2)
;;   "Return the number of lines between `pos1' and `pos2'

;; Avoid the off-by-one error of `count-lines'"
;;   (abs (- (line-number-at-pos pos1)
;; 	  (line-number-at-pos pos2))))

(defun tv/-ivy-same-line-p (ind1 ind2)
  "Return t if the matches ind1 and ind2 are on the same line"
  (with-ivy-window
    (= 1 (count-screen-lines 
	  (nth ind1 ivy--all-candidates)
	  (nth ind2 ivy--all-candidates)
	  t))))

(defun tv/ivy-next-line (&optional arg)
  "Move cursor vertically down ARG candidatkes."
  (interactive "p")
  (setq arg (or arg 1))
  (if (eq (ivy-state-caller ivy-last) 'swiper-isearch)
      (let ((step (signum arg))
	    (i 0))
	(while (not (= i arg))
	  (let ((index (+ ivy--index step)))
	    (cl-incf i step)
            (while (and (< index (1- ivy--length))
			(tv/-ivy-same-line-p index ivy--index))
	    ;; Go to previous match if arg<0, next otherwise
	      (cl-incf index step))
            (ivy-set-index index))))
    (ivy-next-line arg)))

(defun tv/ivy-previous-line (&optional arg)
  (interactive "p")
  (setq arg (or arg 1))
  (tv/ivy-next-line (- arg)))

;; FIXME: Handle the case where the previous value is nil, or where
;; the user might want to toggle with char-fold or other
(defun tv/ivy-toggle-regexp ()
  "Toggle regexp searching on/off in ivy"
  (interactive)
  (if (eq search-default-mode t)
      (setq-local search-default-mode #'regexp-quote)
    (setq-local search-default-mode t)))

(provide 'tv-swiper)

