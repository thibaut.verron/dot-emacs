;;; tv-sage.el --- Custom functions for working with sage code  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Thibaut Verron

;; Author: Thibaut Verron <thibaut@vif-argent>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; Compiler

(declare-function python-indent-line "python.el")
(declare-function python-indent-shift-right "python.el")

(declare-function sage-shell:-doctest-lines "sage-shell-mode")
(declare-function sage-shell-edit:send-region "sage-shell-mode")
(declare-function sage-shell-edit:send-line* "sage-shell-mode")

(require 'tv-defuns)

;; Functions
(defun tv/sage-shell-paste-and-filter ()
  ;; Check indentation level and paste at correct level.
  (interactive)
  (python-indent-line)
  (let ((indent (+ (current-indentation) 4)))
    (indent-line-to 0)
    (let ((beg (point)))
      (yank)
      (let ((end (point)))
        (save-excursion
          (save-restriction
            (narrow-to-region beg end)
            (goto-char (point-min))
            (while (re-search-forward "^-+
\\([^ ]*\\)[[:blank:]]+\\(Traceback (most recent call last)\\)" nil t)
              (let ((tb (match-string 2))
                    (err (match-string 1)))
                (replace-match (concat tb ":"))
                (insert "\n...\n")
                (let ((beg (point))
                      (end (progn (re-search-forward (concat "^" err ":"))
                                  (match-beginning 0))))
                  (delete-region beg end))))
            (python-indent-shift-right (point-min)
                                       (point-max)
                                       indent)
            (goto-char (point-max))
            (forward-line 0)
            (when (looking-at "^[[:blank:]]*sage:[[:blank:]]*$")
              (let ((kill-ring nil))
                (kill-line 2)))
            ;; (insert-char 32 indent)
            ))))))

(defun tv/sage-shell-newline-and-indent ()
  ;; Newline and indent, with consideration for docstrings
  (interactive)
  (let ((in-doctest (sage-shell:-doctest-lines))
        (prev-indent (current-indentation)))
    (newline-and-indent)
    (when in-doctest
      (indent-to-column prev-indent) ;; Why isn't that automatic?
      (insert "sage: ")
      )
    ))
  
(defun tv/sage-shell-send (beg end)
  (interactive
   ;; https://stackoverflow.com/a/13282015/1083706
   (if (use-region-p)
       (list (region-beginning) (region-end))
     (list nil nil)))
  (if end
      (progn
	(sage-shell-edit:send-region beg end)
	(deactivate-mark))
    (progn
      (skip-chars-forward "\n[:space:]")
      (sage-shell-edit:send-line*)
      (forward-line 1))))

(defun tv/sage-shell-other-window ()
  (interactive)
  (tv/eval-other-window '(sage-shell:run-sage)))


(provide 'tv-sage)
;;; tv-sage.el ends here

