(defcustom tv/org-bibtex-yank-level 4 "")

(defvar tv/latex-to-org-replacement-table
  '(("{\\\"\\([aeiouyAEIOUY]\\)}" . "\\\\\\1uml{}")
    ("{\\\\'\\([aeiouyAEIOUY]\\)}" . "\\\\\\1acute{}")
    ("{\\\\`\\([aeiouyAEIOUY]\\)}" . "\\\\\\1grave{}")
    ("{\\\\^\\([aeiouyAEIOUY]\\)}" . "\\\\\\1circ{}")
    ("{\\\\s\\([scSC]\\)}" . "\\\\\\1cedil{}")
    ;; From here to next: move the brace forward
    ("\\\"{\\([aeiouyAEIOUY]\\)}" . "\\\\\\1uml{}")
    ("\\\\'{\\([aeiouyAEIOUY]\\)}" . "\\\\\\1acute{}")
    ("\\\\`{\\([aeiouyAEIOUY]\\)}" . "\\\\\\1grave{}")
    ("\\\\^{\\([aeiouyAEIOUY]\\)}" . "\\\\\\1circ{}")
    ("\\\\s{\\([scSC]\\)}" . "\\\\\\1cedil{}")
    ;; Remove the brace
    ("\\\"\\([aeiouyAEIOUY]\\)" . "\\\\\\1uml{}")
    ("\\\\'\\([aeiouyAEIOUY]\\)" . "\\\\\\1acute{}")
    ("\\\\`\\([aeiouyAEIOUY]\\)" . "\\\\\\1grave{}")
    ("\\\\^\\([aeiouyAEIOUY]\\)" . "\\\\\\1circ{}")
    ("\\\\s\\([scSC]\\)" . "\\\\\\1cedil{}")
    ;; No more accents
    ;; Maybe more replacements?
    ;; After everything is done we can remove braces
    ("{\\([^}]\\)" . "\\1")
    ("\\([^{]\\)}" . "\\1")
    ;; And eventual extraneous backslashes
    ("\\\\\\\\" . "\\\\")
    ))

(defun tv/latex-to-org-string (string)
  (dolist (repl tv/latex-to-org-replacement-table string)
	  (setq string (replace-regexp-in-string (car repl) (cdr repl) string t))))

(defun tv/org-format-title (entry)
  (let ((title (cdr (assq :title entry))))
    (tv/latex-to-org-string title)
    ))

(setq org-bibtex-headline-format-function 'tv/org-format-title)

(defun tv/org-bibtex-write ()
  "Insert a heading built from the first element of `org-bibtex-entries'."
  (interactive)
  (when (= (length org-bibtex-entries) 0)
    (error "No entries in `org-bibtex-entries'"))
  (let* ((entry (pop org-bibtex-entries))
	 (org-special-properties nil) ; avoids errors with `org-entry-put'
	 (val (lambda (field) (cdr (assoc field entry))))
	 (togtag (lambda (tag) (org-toggle-tag tag 'on))))
    (org-insert-heading nil nil t)
    (dotimes (i (- tv/org-bibtex-yank-level 1))
      (org-demote))
    (insert (funcall org-bibtex-headline-format-function entry))
    (org-bibtex-put "TITLE" (funcall val :title))
    (org-bibtex-put org-bibtex-type-property-name
		    (downcase (funcall val :type)))
    (dolist (pair entry)
      (pcase (car pair)
	(:title    nil)
	(:type     nil)
	(:key      (org-bibtex-put org-bibtex-key-property (cdr pair)))
	(:keywords (if org-bibtex-tags-are-keywords
		       (dolist (kw (split-string (cdr pair) ", *"))
			 (funcall
			  togtag
			  (replace-regexp-in-string
			   "[^[:alnum:]_@#%]" ""
			   (replace-regexp-in-string "[ \t]+" "_" kw))))
		     (org-bibtex-put (car pair) (cdr pair))))
	(_ (org-bibtex-put (car pair) (cdr pair)))))
    (mapc togtag org-bibtex-tags)))

(defun tv/org-bibtex-yank ()
  "If kill ring holds a bibtex entry yank it as an Org headline."
  (interactive)
  (let (entry)
    (with-temp-buffer (yank 1) (setf entry (org-bibtex-read)))
    (if entry
	(tv/org-bibtex-write)
      (error "Yanked text does not appear to contain a BibTeX entry"))))


(provide 'tv-org-bibtex)
