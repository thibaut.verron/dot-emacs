;;(setq debug-on-error t)

;; Adapted and modified from https://github.com/seagle0128/.emacs.d/ and
;; https://www.reddit.com/r/emacs/comments/gv3azx/weekly_tipstricketc_thread/ft94scb/
;; (defvar default-file-name-handler-alist file-name-handler-alist)
;; (setq file-name-handler-alist nil)
;; (setq gc-cons-threshold 80000000)

(defvar tv/init-done nil)

;; Just keeping this for esthetic reasons
;; Actually removing: it blocks 
(unless (or debug-on-error tv/init-done)
  (setq inhibit-redisplay t))


(add-hook
 'emacs-startup-hook
 (defun tv/restore-default-values ()
   "Restore default values after init."
   ;; (setq file-name-handler-alist default-file-name-handler-alist)
   ;; (setq gc-cons-threshold 800000)
   (setq inhibit-redisplay nil)
   (setq tv/init-done t)
   (redisplay t)))

(add-to-list 'load-path (concat user-emacs-directory "elisp"))
(add-to-list 'load-path (concat user-emacs-directory "sub-inits"))

;; https://www.reddit.com/r/emacs/comments/cdei4p/failed_to_download_gnu_archive_bad_request/
(when (version< emacs-version "26.3")
  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

;; Capabilities

(defconst tv/emacs-has-modules (not (eq (bound-and-true-p module-file-suffix) nil)))
(defconst tv/emacs-has-xwidgets (featurep 'xwidget-internal))
(defconst tv/emacs-has-native-comp
  (and (fboundp 'native-comp-available-p) (native-comp-available-p)))

;; Taken from DOOM
(defconst tv/is-linux   (eq system-type 'gnu/linux))
(defconst tv/is-windows (memq system-type '(cygwin windows-nt ms-dos)))

;; Early settings

(defconst tv/home-directory (getenv "HOME"))
(defconst tv/user-local-emacs-directory
  (expand-file-name ".emacs-local.d/" tv/home-directory))

(setq custom-file (concat tv/user-local-emacs-directory "custom.el"))

(defgroup tv/custom nil "Personal settings" :prefix "tv/")

;; Those things should be set even if there is an error later

;; Taken from DOOM
(when (fboundp 'set-charset-priority)
  (set-charset-priority 'unicode))       
(prefer-coding-system 'utf-8)            
(setq locale-coding-system 'utf-8)       
(unless tv/is-windows
  (setq selection-coding-system 'utf-8)) 


;; (prefer-coding-system 'utf-8-unix)
;; (set-default-coding-systems 'utf-8-unix)
;; (set-terminal-coding-system 'utf-8-unix)
;; (set-keyboard-coding-system 'utf-8-unix)


(setq 
 ring-bell-function 'ignore  ; No bell at all
 x-select-enable-clipboard t ; Share clipboard between desktop and emacs
 save-interprogram-paste-before-kill t) ; Don't erase desktop clipboard from emacs

;; Packages

(defconst tv/system-type (replace-regexp-in-string "/" "-" (format "%s" system-type)))
(defconst tv/system-string (concat tv/system-type "-" emacs-version))

(setq straight-use-package-by-default t  
      straight-recipes-gnu-elpa-use-mirror t
      straight-build-dir (expand-file-name
			  (concat "build" "-" tv/system-string)
			  tv/user-local-emacs-directory)
      straight-repository-branch "develop")

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el"
	       user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Additional settings for prettier macros
(defconst tv/straight-use-package-font-lock-keywords
  '(("(\\(straight-use-package\\)\\_>[ \t']*\\(\\(?:\\sw\\|\\s_\\)+\\)?"
     (1 font-lock-keyword-face)
     (2 font-lock-constant-face nil t))))

(font-lock-add-keywords 'emacs-lisp-mode
			tv/straight-use-package-font-lock-keywords)

;; Load use-package
(straight-use-package 'use-package)

;; FIXME (upstream): needs to be before the require, so should use a
;; customize setter
(setq use-package-enable-imenu-support t) 
(require 'use-package)

;; (use-package use-package-ensure-system-package)


;; Avoid cluttering .emacs.d with files
(use-package no-littering
  :init
  (progn
    ;; Most of those files should not be shared between computers.
    ;; So we place them outside of the synced .emacs.d directory.
    (setq no-littering-etc-directory
	  (expand-file-name "etc" tv/user-local-emacs-directory))
    (setq no-littering-var-directory
	  (expand-file-name "var" tv/user-local-emacs-directory))
    (require 'no-littering))
  :config
  (with-eval-after-load 'recentf
    (add-to-list 'recentf-exclude no-littering-var-directory)
    (add-to-list 'recentf-exclude no-littering-etc-directory))
  (setq auto-save-file-name-transforms
	`((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))



;; Libraries

(use-package s)

(use-package f
  :init (require 'f))

(use-package dash)

;; Shortcuts to init files

(load "init-shortcuts.el")

;; Personal settings, begin

(load tv/personal-pre-init-file t)
(load tv/local-pre-init-file t)



(defvar tv/theme-style 'light) ; light or dark

;; Load and compile org before anything else
;; (straight-use-package 'org)
;; (straight-use-package 'org-contrib)
;;  '(org
;;    ;; :repo "https://code.orgmode.org/bzg/org-mode.git"
;;    ;; :local-repo "org"
;;    ;; :no-byte-compile t
;;    ;; :no-autoloads t
;;    ))
;; ;(straight-use-package 'org-plus-contrib)

(straight-use-package
 '(org-plus-contrib
   :repo "https://code.orgmode.org/bzg/org-mode.git"
   ;; :local-repo "org"
   ;; :no-byte-compile t
   ;; :no-autoloads t
   ;; :type git
   ;; :host github
   ;; :repo "bzg/org-mode"
   ;; :build (:not autoloads)
   ;; :includes org
   ;; :files (:defaults "contrib/lisp/*.el")
   ))


;; Shorten modelines
(use-package delight
  :init (require 'delight))

;;;; Various graphical settings

;; Coding system

(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment 'utf-8)

					; Size of the initial frame
(if tv/is-windows
    (setq initial-frame-alist
	  '((width . 90) ; chasr
	    (height . 51) ; lines, no idea what unit
	    (left . 50)
	    (top . 50)))
  (setq initial-frame-alist
	'((width . 90) ; chars
	  (height . 90) ; lines, no idea what unit
	  (left . 50)
	  (top . 50))))

(when (eq tv/theme-style 'light)
  (add-to-list 'initial-frame-alist '(background-color . "white"))
  (add-to-list 'initial-frame-alist '(cursor-color . "red")))

(when (eq tv/theme-style 'dark)
  (add-to-list 'initial-frame-alist '(cursor-color . "yellow")))


(setq default-frame-alist initial-frame-alist)


(set-fringe-mode '(10 . 1))

; Avoid waiting time at startup
(modify-frame-parameters nil '((wait-for-wm . nil)))

; Cursor
(setq-default cursor-type '(bar . 2))
(setq-default cursor-in-non-selected-windows '(hbar . 1))

; Better unicode characters

(use-package unicode-fonts
  :defer nil
  :init (unicode-fonts-setup))

;; Custom functions

;;(byte-recompile-directory (concat user-emacs-directory "elisp") 0)

;; Compile all custom functions
;; (let ((srcdir (concat user-emacs-directory "elisp/"))
;;       (compiledir (concat tv/user-local-emacs-directory "elisp/")))
  
;;       )

;; (defun tv/byte-compile-dest-file-function (filename)
;;   (let ((base (file-name-base filename))
;; 	(dir (file-name-directory filename))
;; 	(ext (file-name-extension filename)))
;;     (concat dir base "--" tv/system-string ".elc")))

(defun tv/load-elisp (filename)
  (let* ((absname (expand-file-name
		   filename
		   (concat user-emacs-directory "elisp/")))
	 (base (file-name-nondirectory absname))
	 (destdir (concat tv/user-local-emacs-directory "elisp/"))
	 (dest (concat destdir base)))
    (make-directory destdir t)
    (if tv/is-windows
	(copy-file absname dest 'overwrite)      (unless (file-exists-p dest)
	(make-symbolic-link absname dest)))
    (byte-recompile-file dest nil 0 t)))

(tv/load-elisp "tv-defuns.el")


;; Interaction with desktop environment and window manager

(with-eval-after-load 'tv/gui
  (defconst tv/is-i3
    (and tv/is-linux (= (call-process "pgrep" nil nil nil "-x" "i3") 0)))
  (when tv/is-i3
    (use-package i3
      :init
      (require 'i3)
      (require 'i3-integration)
      :config
      ;; Does not really work because it doesn't create a new frame when
      ;; preventing a new window
      (i3-one-window-per-frame-mode-off) 
      (i3-advise-visible-frame-list-on)
      )
    ;; ;; Has bug eg with reftex-reference
    ;; (use-package frames-only-mode
    ;;   :init (frames-only-mode t))
    ;; (tv/load-elisp "tv-i3.el")
    ;; (bind-key "C-x 3" #'tv/i3-split-horizontal)
    ;; (bind-key "C-x 2" #'tv/i3-split-vertical)
    ;; (bind-key "C-x o" #'tv/i3-other-window)
    ;; (bind-key "C-x 0" #'tv/i3-delete-window-or-frame)
    ))

;; Font, etc

;; We put it in a defun and in a hook to accomodate for daemon mode
;; https://emacs.stackexchange.com/questions/12351/when-to-call-find-font-if-launching-emacs-in-daemon-mode

(defun tv/first-graphical-frame-hook-function ()
  ;; (tv/setup-font)
  (remove-hook 'focus-in-hook #'tv/first-graphical-frame-hook-function)
  (provide 'tv/gui))
(add-hook 'focus-in-hook #'tv/first-graphical-frame-hook-function)


(tv/load-elisp "font-size.el")
(add-hook 'after-make-frame-functions #'tv/adjust-font-size)
(add-hook 'move-frame-functions #'tv/adjust-font-size)


(with-eval-after-load 'tv/gui
  ;;;; Setup the fonts
  ;;;;
  ;; In each block, the fonts are tried in order until one is found on the system.
  ;;;;

  (message "Setting fonts...")
  ;; Monospace fonts for programming
  (cond 
   ;; ((find-font (font-spec :family "Source Code Pro")) ;; Cross-platform, 3rd party
   ;;  (set-face-attribute 'fixed-pitch nil :font "Source Code Pro" :height 105))
   ;; ((find-font (font-spec :family "Input")) ;; Cross-platform, 3rd party
   ;;  (set-face-attribute 'fixed-pitch nil :font "Input" :weight 'normal :height 105))
   ((find-font (font-spec :family "Hack")) ;; Cross-platform, 3rd party
    (set-face-attribute 'default nil :family "Hack" :height 105))
   ((find-font (font-spec :family "Inconsolata")) ;; Cross-platform, 3rd party
    (set-face-attribute 'default nil :family "Inconsolata" :height 120))
   ((find-font (font-spec :family "Consolas")) ;; Windows, default
    (set-face-attribute 'default nil :family "Consolas" :height 110))
   ((find-font (font-spec :family "DejaVu Sans Mono")) ;; Linux, default
    (set-face-attribute 'default nil :family "DejaVu Sans Mono" :height 105)))

  ;; (set-face-attribute 'fixed-pitch nil :inherit 'default)
  (copy-face 'default 'fixed-pitch)
  
  ;; Standard fonts for writing
  (cond
   ;; ((find-font (font-spec :family "InputSans")) ;; Cross-platform, 3rd party
   ;;  (set-face-attribute 'variable-pitch nil :font "InputSans Narrow" :height 105))
   ;; ((find-font (font-spec :family "Iosevka Aile")) ;; Linux, 3rd party
   ;;  (set-face-attribute 'variable-pitch nil :font "Iosevka Aile" :height 1.10))
   ((find-font (font-spec :family "Noto Sans")) ;; Linux, default on KDE
    (set-face-attribute 'variable-pitch nil :family "Noto Sans" :height 0.85 :weight 'normal
			))
   ((find-font (font-spec :family "Carlito")) ;; Linux, 3rd party (Calibri clone)
    (set-face-attribute 'variable-pitch nil :font "Carlito" :height 1.05))
   ((find-font (font-spec :family "DejaVu Sans")) ;; Linux, default
    (set-face-attribute 'variable-pitch nil :font "DejaVu Sans" :height 0.85
			))
   ((find-font (font-spec :family "Ubuntu")) ;; Linux, worse antialiasing than DejaVu
    (set-face-attribute 'variable-pitch nil :font "Ubuntu Light" :height 1.0))
   ((find-font (font-spec :family "Calibri")) ;; Windows (MS office)
    (set-face-attribute 'variable-pitch nil :font "Calibri" :height 1.0))
   ((find-font (font-spec :family "Arial")) ;; Windows
    (set-face-attribute 'variable-pitch nil :font "Arial" :height 0.85)))

  (tv/adjust-font-size nil)

  (use-package fixed-pitch
    :straight (:type git :host github :repo "cstby/fixed-pitch-mode")
    :custom
    (fixed-pitch-blacklist-hooks
     '(comint-mode-hook))
    (fixed-pitch-dont-change-cursor t)
    (fixed-pitch-use-extended-default t)
    :config
    (setq-default cursor-type 'bar))
  
  (message "Setting fonts... done")

  (provide 'tv/faces)
  )
 
;; If the font is too small or should be changed, either use the
;; customize interface, or add new settings in the file
;; .emacs.d/personal-post.el (for a global setting) or .emacs-local.el
;; (for a setting specific to that machine)


;; Theme
;; (use-package leuven-theme
;;     :init (load-theme 'leuven t)
;;   :custom ((leuven-scale-outline-headlines . nil)))


(defun tv/adjust-line-number-face ()
  (set-face-attribute 'line-number nil :height 0.9)
  (set-face-attribute 'line-number-current-line nil :height 'unspecified))

(if (version< emacs-version "27.1")
    (use-package modus-operandi-theme
		 :if (eq tv/theme-style 'light)
		 :init (load-theme 'modus-operandi t))
  (use-package modus-themes
    :if (eq tv/theme-style 'light)
    :straight ( :type git
		:host gitlab
		:repo "protesilaos/modus-themes")
    :init
    (setq modus-themes-subtle-line-numbers nil
	  modus-themes-fringes 'subtle)
    (add-hook 'modus-themes-after-load-theme-hook #'tv/adjust-line-number-face)
    (modus-themes-load-themes)
    (modus-themes-load-operandi)))


;; (use-package tango-dark-theme
;;   :straight (:type built-in)
;;   :if (eq tv/theme-style 'dark)
;;   :init (load-theme 'tango-dark t))

(use-package doom-themes
  :if (eq tv/theme-style 'dark)
  :init (load-theme 'doom-one t))

;(use-package tramp-theme
;  :init (load-theme 'tramp t)
;  :custom
;  (tramp-theme-face-remapping-alist
;   ((".*" nil (mode-line-buffer-id (:inherit mode-line-buffer-it :foreground "Orange")))
;    (nil "^root$" (mode-line-buffer-id (:inherit mode-line-buffer-it :foreground "Red")))
;    )))

;; Better to have another function, now that themes can also make small changes
(defun tv/replace-theme (theme)
  "Like load-theme, but cleanup to make sure the theme is not messing with other themes."
  (mapc #'disable-theme custom-enabled-themes) ;; TODO: add a white list
  (load-theme theme t)
  (tv/adjust-line-number-face)
)

;; (defadvice load-theme (before theme-dont-propagate activate)
;;   (mapc #'disable-theme custom-enabled-themes))

;; (defadvice load-theme (after theme-dont-propagate activate)
;;   (tv/adjust-line-number-face)
;;   )


;; Bindings, etc

(use-package key-combo
  :init (global-key-combo-mode 1))

;; Integration with the desktop environment

;; Scrolling

(bind-key [mouse-4] 'mwheel-scroll)
(bind-key [mouse-5] 'mwheel-scroll)

;; The next two lines would be for pixel-scroll-mode
; (setq mouse-wheel-scroll-amount '(2 ((shift) . 5) ((control) . 10)))
; (setq mouse-wheel-progressive-speed nil)
(setq scroll-margin 5)
(setq scroll-conservatively 1000)


;; Other mouse bindings

(use-package mouse3
  :bind ([mouse-3] . mouse3-popup-menu))

;; Is recursive edit really useful, or is it just confusing?

(add-hook 'mouse-leave-buffer-hook #'tv/stop-using-minibuffer)

;; Show the current line
;; Has performance issues

(use-package hl-line
  :straight (:type built-in)
  :init (global-hl-line-mode t))

;; And indentation in some modes (e.g. elpy)

(use-package highlight-indentation
  :custom
  (highlight-indentation-blank-lines t))

;; Movement

(global-visual-line-mode t)

;; Better beginning and end of buffer
(use-package beginend
  :init (beginend-global-mode))

;;;; Utilities

;; Better defaults

;; Some of those settings are taken from the package `better-defaults':
;; https://github.com/technomancy/better-defaults
(use-package emacs
  :straight (:type built-in)
  :config
  (setq require-final-newline t
	mouse-yank-at-point t
	load-prefer-newer t
	ad-redefinition-action 'accept ; No warning for legacy advice
	apropos-do-all t
	auto-mode-case-fold nil ; Just type the mode name correctly
	mark-even-if-inactive nil ; Don't accidentally delete an inactive region
	)
  (fset 'yes-or-no-p 'y-or-n-p) ; Shorter yes or no prompt
  (transient-mark-mode t)
  (delete-selection-mode t)
  
  (savehist-mode t)
  
  (setq auto-window-vscroll nil) ;; Faster vertical scroll
  
  ;; Those keys are used for the secondary selection. I never needed
  ;; to use it and they sometimes create an annoying state with an
  ;; overlay that is hard to remove. So for now I disable the
  ;; feature.
  ;; FIXME: Can it be done in :bind?
  (global-unset-key [M-mouse-1])
  (global-unset-key [M-drag-mouse-1])
  (global-unset-key [M-down-mouse-1])
  (global-unset-key [M-mouse-3])
  (global-unset-key [M-mouse-2])
  (global-unset-key [f2])
  
  :bind (;; Dangerous and annoying bindings
	 ("C-z" . tv/warn-about-C-z)
	 ("C-x C-z" . nil)
	 ("C-M-z" . suspend-frame) ; Better than C-x C-z
	 ("C-x DEL" . nil)  ; Dangerous
 	 ("<insert>" . nil) ; Annoying

	 ;; Better version of common bindings
	 ("RET" . newline-and-indent)
	 ("C-x C-b" . ibuffer) ; Rebound to `bufler' later
	 ("C-x C-d" . dired)
	 ("M-m" . move-beginning-of-line)
	 ("C-a" . tv/back-to-indentation-or-to-bol)
	 ("C-k" . tv/kill-or-join-line)
	 ("M-u" . upcase-dwim)
	 ("M-l" . downcase-dwim)
	 ("M-c" . capitalize-dwim)
	 
	 ;; Missing functions
	 ("C-x w" . tv/copy-whole-buffer)
	 ("C-x 4 M-x" . tv/M-x-other-window) ; Do something in next window

	 ;; Better bindings for useful functions
	 ("C-=" . repeat)
	 ("C-h" . backward-delete-char-untabify) ;; F1 is help
	 
	 ;; Outside-like bindings
	 ;; See also cua-mode
	 ("C-S-c" . kill-ring-save)
	 ("C-S-v" . yank)
	 ("C-S-x" . kill-region)
	 ("C-S-a" . mark-whole-buffer)
	 ("C-S-s" . save-buffer)
	 ("C-S-z" . undo)
	 ("C-S-f" . isearch-forward) ; Overridden by smartparens

	 ;; Freeing C-v and M-v
	 ;; M-p and M-n are too useful in comint buffers so we keep the main
	 ;; bindings on shifted letters
	 ;; Also, down and up are inverted here because emacs
	 ("M-p" . scroll-down-line)
	 ("M-n" . scroll-up-line)
	 ("M-P" . scroll-down-command)
	 ("M-N" . scroll-up-command)
	 ))

;; Some better functions than the defaults

(use-package crux
  :bind (("C-o" . open-line)
	 ("C-S-o" . crux-smart-open-line)
	 ("C-x 4 t" . crux-transpose-window)))

;; Better backspace
;; FIXME: Get it to kill instead of deleting?
;; FIXME: Handle end-of-line comments?
(use-package smart-hungry-delete
  :bind (([remap backward-delete-char-untabify] . smart-hungry-delete-backward-char)
	 ([remap delete-backward-char] . smart-hungry-delete-backward-char)
	 ([remap delete-char] . smart-hungry-delete-forward-char))
  :init (smart-hungry-delete-add-default-hooks)
  :config
  ;; Fix bug where an additional character is deleted (see https://github.com/hrehfeld/emacs-smart-hungry-delete/issues/12)
  (put #'smart-hungry-delete-backward-char 'delete-selection nil)
  (put #'smart-hungry-delete-forward-char 'delete-selection nil))

;; Fill some bindings when there is no region
;; (use-package whole-line-or-region
;;  :init (whole-line-or-region-global-mode 1))

;; Avoid slowdowns with garbage collection

(use-package gcmh
  :init (gcmh-mode 1)
  :custom
  ;; Silent
  (gcmh-verbose nil) 
  ;; Saner default value (140MB instead of 1GB)
  (gcmh-high-cons-threshold #x8000000))


;; Investigate slowdowns

(use-package explain-pause-mode
  :straight
  (explain-pause-mode
   :type git
   :repo "lastquestion/explain-pause-mode"
   :host github)
  :init (explain-pause-mode -1)
  :custom
  ;; Avoid too many messages, potentially blocking important messages
  (explain-pause-alert-via-message nil))


;; Buffers
(use-package uniquify
  :straight (:type built-in)
  :custom
  (uniquify-buffer-name-style 'post-forward-angle-brackets))




;; More complete help (including keymaps)

(use-package help-fns+)

;; Session, etc

(use-package emacs
  :config (save-place-mode t))

(use-package recentf
  :init (recentf-mode 1)
  :custom
  (recentf-max-saved-items 25)
  :config
  (tv/load-elisp "recentf-plus.el"))


;; Dashboard

(use-package dashboard
  :custom
  (dashboard-items '((recents  . 10) (bookmarks . 5) (projects . 5)
		       (agenda . 5) ;; (registers . 5)
		       ))
  :init
  (dashboard-setup-startup-hook)
  :config
  (when (daemonp)
    (setq initial-buffer-choice
	  (lambda ()
	    (if (< (length command-line-args) 2)
		(get-buffer dashboard-buffer-name)))))
  (defun tv/dashboard-revert-buffer (ignoreauto noconfirm)
    (dashboard-refresh-buffer))
  (add-hook 'dashboard-mode-hook
	    (defun tv/dashboard-setup-revert-function ()
	      (setq-local revert-buffer-function #'tv/dashboard-revert-buffer))))

;; Counsel, etc

;; counsel-M-x uses smex if available
(use-package smex)


(defvar tv/using-swiper-fork nil)
(defvar tv/path-to-swiper-fork (f-join user-emacs-directory "devel/forks/swiper"))

(if tv/using-swiper-fork
    (use-package swiper
      :straight nil
      :load-path tv/path-to-swiper-fork)
  (use-package swiper))

(defun tv/ivy-action-insert-relative (x)
  (let ((filename
	 (if (stringp x)
	     (ivy--trim-grep-line-number x)
	   x (car x))))
    (insert (file-relative-name filename))))

(use-package ivy
  :init (ivy-mode 1)
  :custom
  (ivy-use-virtual-buffers t)
  (ivy-display-style 'fancy)
  (ivy-use-selectable-prompt t)
  (ido-file-extensions-order
   '(;; Meta files
     ".org" ".txt"
     ;; Main files
     ".tex" ".bib" ".pdf"
     ".mpl" ".m" ".mgm" 
     ".py" ".pyx" ".sage"
     ".c" ".h" ".cpp" ".el"
     ;; Auxiliary files
     ".bbl" ".log" ".aux"
     ;; Rest
     "" t))
  :config
  (setcdr (assq 'read-file-name-internal ivy-sort-functions-alist)
	  'ido-file-extension-lessp))
  

(use-package counsel
  :init (counsel-mode 1)
  :custom (counsel-find-file-at-point t)
  :config
  (ivy-add-actions
   #'counsel-find-file
     '(("I" tv/ivy-action-insert-relative "insert relative file name")))
  (push '(counsel-minibuffer-history . nil)
	 ;; default sort = list order: most recently added first
	 ivy-sort-functions-alist)
  (push '(counsel-imenu . nil)
	 ;; default sort = list order: most recently added first
	 ivy-sort-functions-alist)
  (push '(counsel-shell-history . nil)
	 ;; default sort = list order: most recently added first
	 ivy-sort-functions-alist)
  :bind (nil
	 :map counsel-mode-map
	 ("<f1> o" . counsel-describe-symbol)
	 ;; ;; Disabled until they can be compatible with -other-window
	 ;; ([remap switch-buffer] . counsel-switch-buffer)
	 ;; ([remap ivy-switch-buffer] . counsel-switch-buffer)
	 ("C-x b" . ivy-switch-buffer)
	 ("C-x 4 C-b" . ivy-switch-buffer-other-window)

	 :map minibuffer-local-map
	 ("M-y" . yank-pop) ; Can't have the counsel version in minibuffer
	 ("C-r" . counsel-minibuffer-history)

	 :map comint-mode-map
	 ("C-r" . counsel-shell-history)
	 ))

;; Similar to smex but for files
(use-package ivy-prescient
  :custom
  (prescient-persist-mode t)
  :init (ivy-prescient-mode 1))



(use-package swiper
  :init ;(require 'swiper)
  :custom
  ;; We shouldn't need to escape backslashes for a simple search
  (search-default-mode #'regexp-quote)
  ;; Improve performance with long lines
  (swiper-use-visual-line-p #'ignore) 
  :config
  ;; Sort files according to extensions
  (tv/load-elisp "tv-swiper.el")
  :bind(([remap isearch-forward] . swiper-isearch)
        ("C-r" . swiper-isearch-backward)
        ;("M-r" . ivy-reverse-i-search)
        ("C-c C-r" . ivy-resume)
        ("<f6>" . ivy-resume)

	;;;; Following commented out in favor of counsel-mode
        ;; ("M-x" . counsel-M-x)
        ;; ("C-x C-f" . counsel-find-file)
        ;; ("<f1> f" . counsel-describe-function)
        ;; ("<f1> v" . counsel-describe-variable)
        ;; ("<f1> l" . counsel-load-library)
        ;; ("<f2> i" . counsel-info-lookup-symbol)
        ;; ("<f2> u" . counsel-unicode-char)
        ;; ("C-c g" . counsel-git)
        ;; ("C-c j" . counsel-git-grep)
        ;; ("C-c k" . counsel-ag)
        ;; ("C-x l" . counsel-locate)
	;; ("M-y" . counsel-yank-pop)
        :map swiper-map
	;; I like to have C-s and C-r go to the next and previous match, but
	;; with swiper's minibuffer prompt, it makes more sense that C-n and
	;; C-p act on lines in the prompt.
        ("C-r" . ivy-previous-line)
        ("C-s" . ivy-next-line-or-history)
	([remap next-line] . tv/ivy-next-line)
	([remap previous-line] . tv/ivy-previous-line)
	
	("M-r" . tv/ivy-toggle-regexp)
	:map ivy-minibuffer-map
	("M-y" . ivy-next-line)
	))

(use-package ivy-rich
  :init (ivy-rich-mode)
  :custom
  (ivy-rich-parse-remote-buffer nil))

;; Experimental: position of the prompt
;; (use-package ivy-posframe
;;   :init (ivy-posframe-enable)
;;   :custom
;;   (ivy-posframe-style 'point)
;;   (ivy-posframe-width 80)
;;   (ivy-posframe-display-functions-alist
;;    '(
;;      (swiper          . ivy-posframe-display-at-window-bottom-left)
;;      (swiper-isearch  . ivy-posframe-display-at-window-bottom-left)
;;      (swiper-isearch-backward
;;       . ivy-posframe-display-at-window-bottom-left)
;;      (t               . ivy-posframe-display)
;;      )))

;; Better query replace

;;;; There are a lot of choices available. This one offers
;;;; query-replace in region (like default query-replace) and preview
;;;; of the replacement (like vim).

(use-package anzu
    :bind (([remap query-replace] . anzu-query-replace)
	   ([remap query-replace-regexp] . anzu-query-replace-regexp)))

;; Easy search for symbols

(use-package smartscan
  :init (global-smartscan-mode 1) ; Maybe this should only be for prog-modes?
  :custom
  (smartscan-symbol-selector "symbol")
  :bind (nil
	 :map smartscan-map
	 ;; Used for history
	 ("M-p" . nil)
	 ("M-n" . nil)
	 ;; Let's try not to use those so much
	 ("M-<up>" . smartscan-symbol-go-backward)
	 ("M-<down>" . smartscan-symbol-go-forward)
	 ;; Better ones?
	 ("M-r" . smartscan-symbol-go-backward)
	 ("M-s" . smartscan-symbol-go-forward)))

;; Easy jump while we are there

(use-package avy
  :bind ("C-;" . avy-goto-char-timer))

;; And menu-based jump

(use-package imenu
  :straight (:type built-in)
  :bind (("M-`" . imenu)))

;; Completion

;; I don't like intrusive in buffer completion, so I use ivy to complete using company backends
(use-package company
  :delight ""
  :custom ((company-idle-delay nil)  ;; No idle completion
	   (company-dabbrev-char-regexp "\\sw\\|\\s_") ;; Match symbols too
	   (company-dabbrev-other-buffers t)) ;; Only match against buffers with the same mode
  :bind (("C-M-i" . counsel-company)))

;;;; Key completion

(use-package which-key
  :init (which-key-mode 1))

;;;; Movement

(use-package move-text
  :bind (("C-S-<up>" . move-text-up)
	 ("C-S-<down>" . move-text-down)))

;; Git

(use-package vc
  :config (setq vc-follow-symlinks t))

(use-package git-modes)

(use-package magit
  :custom
  (magit-branch-read-upstream-first 'fallback)
  :config
  ;; Be able to use revert-buffer instead of the shortcut g
  (defun tv/magit-revert-buffer (ignoreauto noconfirm)
		      (magit-refresh-buffer))
  ;; (add-hook 'magit-mode-hook
  ;; 	    (defun tv/setup-revert-function ()
  ;; 	      (setq-local revert-buffer-function
  ;; 			  #'tv/magit-revert-buffer)))
  :bind (("C-x g" . magit-status)))

(use-package git-time-machine)

;; Diff and conflict resolution

; We make our own function to decide on the fly depending on whether we are using i3
(defun tv/ediff-window-setup-function (buffer-A buffer-B buffer-C control-buffer)
  (if tv/is-i3
      (ediff-setup-windows-plain buffer-A buffer-B buffer-C control-buffer)
    (ediff-setup-windows-default buffer-A buffer-B buffer-C control-buffer)))

(use-package ediff
  :straight (:type built-in)
  :custom
  (ediff-window-setup-function #'tv/ediff-window-setup-function))

;;; Restore window configuration after quit
;;; Taken from https://emacs.stackexchange.com/a/17089/184
(defvar tv/ediff-last-windows nil)

(defun tv/store-pre-ediff-winconfig ()
  (setq tv/ediff-last-windows (current-window-configuration)))

(defun tv/restore-pre-ediff-winconfig ()
  (set-window-configuration tv/ediff-last-windows))

(add-hook 'ediff-before-setup-hook #'tv/store-pre-ediff-winconfig)
(add-hook 'ediff-quit-hook #'tv/restore-pre-ediff-winconfig)

;; Online editors

(use-package atomic-chrome
  :init (atomic-chrome-start-server)
  :config
  (setq atomic-chrome-url-major-mode-alist
    	'(("overleaf\\.com" . latex-mode)
	  ("moodle\\..*" . text-mode)
	  ;; FIXME: iirc there is a better mode for gmail
	  ("gmail\\.com" . html-mode))))

;; Projects

(use-package projectile
  :init (projectile-mode 1)
  :custom
  (projectile-completion-system 'ivy)
  (projectile-mode-line-prefix " Proj")
  :config
  (add-to-list 'projectile-globally-ignored-directories
	       (concat straight-base-dir "straight/repos"))
  :bind (nil
	 :map projectile-mode-map
	 ("C-x p" . projectile-command-map)))

(use-package counsel-projectile
  :init (counsel-projectile-mode 1)
  :config
  (progn
    (push '(counsel-projectile-find-file . ido-file-extension-lessp)
	  ivy-sort-functions-alist)
    (push '(counsel-projectile-switch-project . ido-file-extension-lessp)
	  ivy-sort-functions-alist)))

;; Folding

;; (use-package hideshow
;;   :straight (:type built-in))

;; (use-package hideshow-org
;;   :hook ((prog-mode-hook . 'hs-org/minor-mode)))

;; Evil for a few perks
;; Vi bindings become available if wanted, I don't use them
;; (use-package evil
;;   :init (evil-mode)
;;   :config
;;   (progn
;;     (defalias 'evil-insert-state 'evil-emacs-state))
;;   :bind (nil
;; 	 :map evil-emacs-state-map
;; 	 ;; FIXME: something for evil-execute-in-normal-state
;; 	 ))

;; (use-package key-chord
;;   :init (key-chord-mode 1)
;;   :config
;;   (progn
;;     (key-chord-define evil-emacs-state-map "jk" #'evil-normal-state)
;;     (defalias 'evil-insert-state 'evil-emacs-state)    ))

;; Mode line: powerline and minions

(use-package powerline
  :straight 
      (powerline
       :type git
       :repo "ThibautVerron/powerline"
       :branch "tramp"
       :host github)
  :config (powerline-default-theme))

(use-package minions
  :init (minions-mode 1)
  :config
  (progn
    (setq minions-direct nil
	  minions-mode-line-lighter "---"
	  minions-mode-line-delimiters '("" . ""))
    (with-eval-after-load 'powerline
      (defpowerline powerline-major-mode "")
      (defpowerline powerline-process "")
      (defpowerline powerline-minor-modes
	minions-mode-line-modes))))

;; Sidebar

(use-package dired-sidebar
  :bind (("<f7>" . dired-sidebar-toggle-sidebar))
  :init
  (add-hook 'dired-sidebar-mode-hook
            (lambda ()
              (unless (file-remote-p default-directory)
                (auto-revert-mode))))
  :config
  (push 'toggle-window-split dired-sidebar-toggle-hidden-commands)
  (push 'rotate-windows dired-sidebar-toggle-hidden-commands)
  (setq dired-sidebar-use-term-integration t))


;; Hydra

(use-package hydra)

;; Switching between windows

;; Customize it to the string of keys on the middle row of your keyboard
;; FIXME: there should be a setter
(defcustom tv/main-row-keys "qsdfghjklm"
  "String of keys on the main row of the keyboard.

This should be adjusted depending on one's keyboard layout."
  :type '(choice (const :tag "QWERTY (US)" "asdfghjkl;")
		 (const :tag "QWERTZ (DE)" "asdfghjklö")
		 (const :tag "AZERTY (FR)" "qsdfghjklm")
		 (const :tag "QWERTY (TR)" "asdfghjklş")
		 (string :tag "Other"))
  :group 'tv/custom)

(use-package ace-window
  :bind (("M-o"   . ace-window))
  :config (setq aw-keys (string-to-list
			 (concat (substring tv/main-row-keys 0 4) ; 4 first characters
				 (substring tv/main-row-keys -4 nil))) ; and 4 last
		aw-scope 'visible
		aw-dispatch-always nil))

(use-package windmove
  :straight (:type built-in)
  :init (require 'windmove)
  :config 
  (windmove-default-keybindings 'shift)
  (setq windmove-wrap-around t)
  ;; :bind (("M-S-<left>"  . windmove-left)
  ;; 	 ("M-S-<right>" . windmove-right)
  ;; 	 ("M-S-<up>"    . windmove-up)
  ;; 	 ("M-S-<down>"  . windmove-down))
  )

;; Buffer list

;; (use-package bufler
;;   :bind ([remap ibuffer] . bufler))

;; Line numbers
;; TODO: Move to use-package

(defvar tv/builtin-line-numbers
  (version<= "26.1" emacs-version))
(setq display-line-number-width t)


(defun tv/turn-on-display-line-numbers ()
  (interactive)
  (if tv/builtin-line-numbers
      (display-line-numbers-mode 1)
    (linum-mode 1)))

(defun tv/turn-off-display-line-numbers ()
  (interactive)
  (if tv/builtin-line-numbers
      (display-line-numbers-mode -1)
    (linum-mode -1)))

(when tv/builtin-line-numbers
  (add-hook 'prog-mode-hook #'tv/turn-on-display-line-numbers))

;; Better form of undo

;; ;; Disabled because not used in a long time
;; (use-package undo-tree
;;   :delight "u")

(use-package undo-fu
  :bind (("C-/" . undo-fu-only-undo)
	 ("C-S-/" . undo-fu-only-redo)
	 ("C-?" . undo-fu-only-redo)))

;; Winner-mode : undo window configuration changes

(use-package winner
  :straight (:type built-in)
  :init (winner-mode 1)
  :bind (("C-c <left>" . winner-undo)
	 ("C-c <right>" . winner-redo)))

;; Syntax coloring

(use-package jit-lock
  :straight (:type built-in)
  :custom
  (jit-lock-context-time 1))

;; Parentheses

(defvar tv/using-smartparens-fork nil)

(if tv/using-smartparens-fork
    (use-package smartparens
      :straight 
      (smartparens
       :type git
       :repo "ThibautVerron/smartparens"
       :branch "feature/generic-string"
       :host github))
  (use-package smartparens))


;; Automatic pairing of parentheses
;; Add the following to the file personal-post.el to disable:
;; (smartparens-global-mode nil) ;; To disable automatic insertion
;; (show-smartparens-global-mode nil) ;; To disable display of parentheses
(use-package smartparens
  :init (require 'smartparens)
  :config
  (progn
    (smartparens-global-mode t)
    (show-smartparens-global-mode t)
    (require 'smartparens-config)
    (add-hook 'minibuffer-setup-hook
              (lambda ()
                (if (eq this-command 'eval-expression)
                    (smartparens-mode 1))))
    ;; Some themes apply a bold effect which looks bad on variable-pitch fonts.
    ;; If not visible enough, you can consider adding :inverse-video t
    (when (eq tv/theme-style 'dark)
      (set-face-attribute 'sp-show-pair-match-face t
			  :weight 'normal :inverse-video t)
      (set-face-attribute 'sp-show-pair-mismatch-face t
			  :weight 'normal :inverse-video t)))
  ;; :custom
  ;; (sp-show-pair-delay 1)
  :bind (("C-S-f" . sp-forward-sexp)
         ("C-S-b" . sp-backward-sexp)
         ("C-S-n" . sp-up-sexp)
         ("C-S-p" . sp-backward-up-sexp)
         ("C-S-w" . sp-copy-sexp)
         ("C-S-k" . sp-kill-hybrid-sexp)
         ("C-S-t" . sp-transpose-hybrid-sexp)
         ("C-S-d" . sp-kill-sexp))) 
;; (require 'smartparens)

;; Utility for working with camel case, etc

(use-package subword
  :straight (:type built-in)
  :delight subword-mode)

;; Easy select block of text surrounding character
(use-package expand-region
  :bind (("C-," . er/expand-region)
	 ("C-." . er/contract-region)))

;; Multiple cursors
(use-package multiple-cursors
  :custom
  (mc/insert-numbers-default 1)
  :bind (("C-<" . mc/mark-previous-like-this)
	 ("C->" . mc/mark-next-like-this)
	 ("C-c C->" . mc/mark-all-like-this)
	 :map mc/keymap
	 ("<return>" . nil)
	 ("C-$" . mc/insert-numbers)))

;; Snippets

(use-package yasnippet
  :init
  (progn
    (setq yas-snippet-dirs (list (concat user-emacs-directory
					 "etc/snippets/")))
    (yas-global-mode))
  :delight "Y"
  :config
  (progn
    (setq yas-indent-line 'auto))
  :bind (nil
	 :map yas-minor-mode-map
	 ; Need all of those..
	 ("<tab>" . nil)
	 ("TAB" . nil)
	 ("C-<tab>" . yas-expand)
	 ("C-TAB" . yas-expand)
	 ("C-c s s" . yas-insert-snippet)
	 ("C-c s n" . yas-new-snippet)
	 ("C-c s f" . yas-visit-snippet-file)
	 :map yas-keymap
	 ("<tab>" . yas-next-field)
	 ("C-<tab>" . yas-expand)))

(use-package ivy-yasnippet
  :bind (nil
	 :map yas-minor-mode-map
	 ("C-c s s" . ivy-yasnippet)))


(use-package yasnippet-snippets
  :init (yasnippet-snippets-initialize))

(use-package auto-yasnippet)


;;;; Text languages, etc

(defcustom tv/small-screen nil
  "Whether this emacs will be run on a device with limited screen space.

If t, various settings such as the margins and the width of the
columns are changed."
  :group 'tv/custom)

(use-package visual-fill-column
  :init (require 'visual-fill-column) ;; We are not setting autoloads
  :custom
  ;; Split the window horizontally even if the buffer has a wide margin due to
  ;; visual-fill-column
  (split-window-preferred-function 
   #'visual-fill-column-split-window-sensibly))

(use-package adaptive-wrap
  :config (add-hook 'visual-line-mode-hook #'adaptive-wrap-prefix-mode))  


(advice-add 'visual-fill-column--set-margins
	    :after
	    #'tv/vfc-set-widths)
  
(defcustom tv/text-fill-column 'auto "" :group 'tv/custom)

(defun tv/text-hook-function ()
  (variable-pitch-mode t)
  (if (eq tv/text-fill-column 'auto)
      (setq-local fill-column (if tv/small-screen 80 90))
    (setq-local fill-column tv/text-fill-column))
  ;;(auto-fill-mode t)
  (visual-line-mode t)
  (visual-fill-column-mode t))

(add-hook 'text-mode-hook 'tv/text-hook-function)

; .txt files are the exception
(setq auto-mode-alist (append '(("\\.txt$" . fundamental-mode))
                              auto-mode-alist))


;; LaTeX

(use-package latex
  :straight auctex
  :mode ("\\.\\(ltx\\|tex\\)" . LaTeX-mode)
  :demand
  :config
  (progn
    (tv/load-elisp "tv-latex.el")
    (load "init-latex.el")))

;; Previewer if supported

(with-eval-after-load 'tv/gui
  (use-package webkit-katex-render
					;:disabled ; Not working so far
    :straight 
    (webkit-katex-render 
     :type git
     :repo "fuxialexander/emacs-webkit-katex-render"
     :host github)
    :if (and tv/emacs-has-modules
	     tv/emacs-has-xwidgets
	     (not tv/is-i3))
    :config
    (setq webkit-katex-render--background-color "white")
    (setq webkit-katex-render--foreground-color "black"))
)

;; Citations

(use-package bibtex
  :config
  (setq bibtex-autokey-year-length 4
	bibtex-autokey-name-year-separator "-"
	bibtex-autokey-name-case-convert 'capitalize
	bibtex-autokey-name-title-separator "-"
	bibtex-autokey-year-title-separator "-"
	bibtex-autokey-titleword-separator ""
	bibtex-autokey-titleword-case-convert-function 'capitalize
	bibtex-autokey-titlewords 3
	bibtex-autokey-titleword-length t
	bibtex-autokey-titlewords-stretch 0
	bibtex-maintain-sorted-entries nil
	bibtex-autokey-titleword-ignore
	'("[aA]" "[Aa]n" "[Oo]n" "[Tt]he" "[Oo]f" "[Aa]nd" "[Ii]s"
	  "[Ll]a" "[Ll]es?" "[Dd]u" "[Dd]es?" "[Uu]ne?")
	;; bibtex-autokey-titleword-ignore nil
	bibtex-completion-pdf-field "file"
	)
  (dolist
      (it '(("[âåàä]" . "a")
	    ("[æ]" . "ae")
	    ("[éëêè]" . "e")
	    ("[îıíï]" . "i")
	    ("[ôóö]" . "o")
	    ("[œ]" . "oe")))
    (add-to-list 'bibtex-autokey-transcriptions it)))



(use-package biblio
  :config
  (setq biblio-bibtex-use-autokey t))

(defun tv/open-bibliography (&optional new-frame)
  (interactive "P")
  (tv/find-file (car bibtex-completion-bibliography) new-frame))

(defalias 'biblio #'tv/open-bibliography)

;; Better completion than the reftex default, slightly nicer interface
(use-package ivy-bibtex
  ;; :init
  ;; (progn
  ;;   (require 'reftex) ; somehow with-eval-after-load makes an error
  ;;   (bind-key "C-c [" 'tv/ivy-bibtex-insert-citation reftex-mode-map))
  :bind (("C-c [" . ivy-bibtex)
	 :map reftex-mode-map
	 ("C-c [" . tv/ivy-bibtex-insert-citation))
  :config
  (setq ivy-re-builders-alist
	'((ivy-bibtex . ivy--regex-ignore-order)
	  (t . ivy--regex-plus))
	ivy-bibtex-default-action 'ivy-bibtex-edit-notes))


  



;; personal settings to customize


;; Reading: org-noter and pdf-tools (if possible)


(defvar tv/load-pdf-tools nil
  "Should we try to install and load `pdf-tools'?

This variable must be set before loading the emacs configuration.")

(defvar tv/load-eaf nil
  "Should we try to install and load `eaf'?

This variable must be set before loading the emacs configuration.")

(defvar tv/load-jupyter nil
  "Should we try to install and load `jupyter'?

This variable must be set before loading the emacs configuration.")


(with-eval-after-load 'tv/gui
  (when tv/load-pdf-tools
    (use-package pdf-tools
      :init
      (condition-case nil
	  (pdf-tools-install)
	(error (message 
		(concat "Installation of `pdf-tools' failed.\n"
			"If you do not want to see this message again,\n"
			"add `(setq tv/load-pdf-tools nil)' "
			"to `.emacs.d/personal-pre.el'"))))
  
      ;; Normally the config only works if the init was successful
      
      :config 
      (progn
	(require 'pdf-sync)

	;; https://emacs.stackexchange.com/a/19475
	;; Use pdf-tools to open PDF files
	(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
	      TeX-source-correlate-start-server t)
	;; Update PDF buffers after successful LaTeX runs
	(add-hook 'TeX-after-compilation-finished-functions
		  #'TeX-revert-document-buffer)

	;; In pdf files, pdf-tools implements its own isearch, which
	;; is not compatible with swiper. Given that `swiper-isearch'
	;; was bound with a remap, binding the new function to
	;; `isearch-forward' is not enough, so we hide it instead
	;; (see the bind below).
	(defalias 'tv/pdf-isearch-forward 'isearch-forward)

	;; Automatically select a size
	;; FIXME: Make this dependent on buffer size
	;; FIXME: Move to a byte-compiled file
	(add-hook 'pdf-view-mode-hook
		  (defun tv/pdf-view-size ()
		    (pdf-view-set-slice-from-bounding-box)
		    (pdf-view-fit-page-to-window))))

      :bind (([remap scroll-other-window] . tv/scroll-other-window)
	     ([remap scroll-other-window-down] . tv/scroll-other-window-down) 

	     :map pdf-view-mode-map
	     ("j" . pdf-view-next-line-or-next-page)
	     ;; k is by default bound to image-kill-buffer which is annoying and
	     ;; dangerous. q is enough if I want to quit
	     ("k" . pdf-view-previous-line-or-previous-page) 
	     
	     :map pdf-isearch-minor-mode-map
	     ("C-s" . tv/pdf-isearch-forward)))
    (when (not (version< emacs-version "27"))
      (use-package pdf-continuous-scroll-mode
	:straight (pdf-continuous-scroll-mode
		   :type git :host github
		   :repo "dalanicolai/pdf-continuous-scroll-mode.el")
	:hook ((pdf-view-mode . pdf-continuous-scroll-mode))
	:bind ( :map pdf-view-mode-map
		("j" . pdf-continuous-scroll-forward)
		("k" . pdf-continuous-scroll-backward)
		("J" . pdf-continuous-next-page)
		("K" . pdf-continuous-previous-page))))))

;; Alternative: application framework
(with-eval-after-load 'tv/gui
  (when tv/load-eaf
    (use-package eaf
      :straight ( eaf
		  :type git
		  :host github
		  :repo "emacs-eaf/emacs-application-framework"
		  :files ("*")
		  :pre-build (("python3" "install-eaf.py" "--install" "pdf-viewer" "--ignore-sys-deps"))
		  )
      ;;:files ("*.el" "*.py" "core" "app" "extensions" "*.json"))
      ;; :init (require 'eaf)
      ))
)


;; Internet

(defvar tv/load-emacs-webkit t)

(with-eval-after-load 'tv/gui
  (when (and (version<= "28" emacs-version) tv/emacs-has-xwidgets tv/load-emacs-webkit)
    (use-package webkit
      :straight (webkit :type git :host github :repo "akirakyle/emacs-webkit"
			:branch "main"
			:files (:defaults "*.js" "*.css" "*.so")
		      :pre-build (("make"))))))

;; Org

(use-package org
  :init
  (setq org-disputed-keys
	'(([(shift up)]		   . [(meta p)])
	  ([(shift down)]          . [(meta n)])
	  ([(shift left)]	   . [(meta -)])
	  ([(shift right)]	   . [(meta =)])
	  ([(control shift right)] . [(meta shift =)])
	  ([(control shift left)]  . [(meta shift -)])))
  (setq org-replace-disputed-keys t)
  :config
  (progn
    (tv/load-elisp "tv-org.el")
    (load "init-org.el"))
  )


(use-package org-noter
  ;:config (setq org-noter-always-create-frame nil)
  )

;; (use-package org-variable-pitch
;;   :straight (org-variable-pitch
;; 	     :type git :host github :repo "cadadr/elisp"
;; 	     :branch "ovp-new-face-setup"
;; 	     :files ("org-variable-pitch.el"))
;;   :after tv/faces
;;   :init
;;   (progn
;;     (message "Configuring org-variable-pitch...")
;;     (org-variable-pitch-setup)
;;     (message "Configuring org-variable-pitch... done"))
  
;;   ;; (setq org-variable-pitch-fixed-font
;;   ;; 	      (face-attribute 'default :family))
;;   :config
;;   ;; (add-hook 'org-mode-hook 'org-variable-pitch-minor-mode)
;;   ;; (set-face-attribute 'org-variable-pitch-face nil
;;   ;; 		      :height 'unspecified)
;;   ;; (set-face-attribute 'org-variable-pitch-face nil
;;   ;; 		      :inherit 'default)
;;   )

;; Does this obsolete OVP?
(use-package valign
  :straight (valign
	     :type git :host github :repo "casouri/valign")
  :after tv/faces
  :custom
  (valign-fancy-bar t)
  :config (add-hook 'org-mode-hook #'valign-mode))


(use-package org-roam
  :if (executable-find "sqlite3")
  :init
  (require 'ucs-normalize) ;; Workaround a bug upstream
  (setq org-roam-directory (file-truename "~/org-roam")
	org-roam-db-location "~/org-roam/org-roam.db")
  (make-directory org-roam-directory t)
  (org-roam-setup)
  ;; :custom
  ;; (org-roam-find-file-function 'counsel-find-file)
  :config

  :bind (;("C-x f" . nil)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n c" . org-roam-capture)
	 ("C-c n r" . org-roam-buffer-toggle)
	 ("C-c n i" . org-roam-node-insert)))

(use-package org-ref)

(use-package org-roam-bibtex
  :init
  (org-roam-bibtex-mode)
  :bind (nil
	 :map org-roam-bibtex-mode-map
	 ("C-c )" . nil)))

;;;; Programming languages and interfaces

;; Better filling
(use-package filladapt
  :init (require 'filladapt)
  :hook
  ((prog-mode
    . (lambda ()
	(auto-fill-mode 1)
	(filladapt-mode 1)
	(setq fill-column 80
	      comment-auto-fill-only-comments t)))))

;; Jump to variable or function definition with M-.

(use-package ivy-xref
:init
  ;; xref initialization is different in Emacs 27 - there are two different
  ;; variables which can be set rather than just one
  (when (>= emacs-major-version 27)
    (setq xref-show-definitions-function #'ivy-xref-show-defs))
  ;; Necessary in Emacs <27. In Emacs 27 it will affect all xref-based
  ;; commands other than xref-find-definitions (e.g. project-find-regexp)
  ;; as well
  (setq xref-show-xrefs-function #'ivy-xref-show-xrefs))

(use-package dumb-jump
  :init (require 'dumb-jump)
  :custom
  (dumb-jump-default-project "./")
  :config
  ;; TODO: Is there a better way of doing this?
  ;; A simple add-hook would work but not when evaluated directly from within an
  ;; elisp buffer because emacs-lisp mode has a buffer-local value of the variable.
  (let ((old (default-value 'xref-backend-functions)))
    (add-to-list 'old #'dumb-jump-xref-activate t)
    (setq-default xref-backend-functions (remq 'etags--xref-backend old))))

;; auto-insert
(use-package emacs
  :straight (:type built-in)
  :config
  (progn
    (setq auto-insert-directory
	  (concat user-emacs-directory "etc/auto-insert/"))))

;; Interaction with processes

(use-package comint
  :straight (:type built-in)
  :config
  ;; Smartparens can become extremely slow in comint buffers
  ;; Native compilation improves that
  (unless tv/emacs-has-native-comp
    (add-hook 'comint-mode-hook
	      (defun tv/comint-hook-function ()
	      ;; Can become extremely slow for no good reason when the buffer
		;; has long output
		(show-smartparens-mode -1)))))

;; Terminal

(use-package vterm
  :if (and tv/emacs-has-modules
	   (executable-find "cmake")
	   (executable-find "libtool")))

;; Alternative
(use-package eshell
  :straight (:type built-in)
  :custom
  (eshell-glob-case-insensitive t)) ;; case-insensitive completion

;; Debugging
;; Disabled: not used, and causes a lot of warnings with
;; (use-package realgud)

;; emacs-lisp

(use-package lisp-mode
  :straight (:type built-in)
  :delight (emacs-lisp-mode ".el")
  :config
  ;; (progn
  ;;   (setq lisp-indent-function #'common-lisp-indent-function))
  :bind (nil
	 :map emacs-lisp-mode-map
	 ("C-c C-c" . eval-buffer)
	 ("C-c C-b" . eval-buffer)
	 ("C-c C-o" . tv/ielm-other-window)
	 ("C-c C-i" . ielm)))

;; Better help buffers for elisp
(use-package helpful
  :config
  (setq counsel-describe-function-function #'helpful-callable
	counsel-describe-variable-function #'helpful-variable))

;; Better version of common keybindings for elisp
(use-package pp
  :bind (("M-:" . pp-eval-expression)
	 ("C-x C-e" . pp-eval-last-sexp)))

;; Tests, etc

(use-package feature-mode
  :mode ("\\.feature\'" . feature-mode))

(use-package ecukes
  :ensure t
  :config (add-hook 'feature-mode-hook 'ecukes-minor-mode))

(use-package faceup)

;; Python, sage, etc

(use-package python
  :delight ".py"
  :custom
  (python-fill-docstring-style 'onetwo)
  (python-pdbtrack-activate nil)
  :preface
  (tv/load-elisp "tv-sage.el")
  :hook
  ((python-mode
    . (lambda ()
	;; Makes input slow
	(delq 'python-indent-post-self-insert-function
	      post-self-insert-hook)
	;; The mode offers a good filling function
	(filladapt-mode -1))))
  :bind (nil
	 :map python-mode-map
	 ("C-c C-y" . tv/sage-shell-paste-and-filter)
	 ("C-c C-j" . tv/sage-shell-newline-and-indent)))

;; Another python mode, provides folding, snippets, etc
;; TODO: Remove duplicate bindings
;; TODO: Remove duplicate snippets
;; FIXME: Folding?
(use-package elpy
  :init
  (progn
    (setq elpy-modules (list 'elpy-module-yasnippet)
	  elpy-mode-map (make-sparse-keymap)
	  elpy-shell-map (make-sparse-keymap))
    (elpy-enable))
  :hook
  ((elpy-mode
    . (lambda ()
	(add-to-list 'xref-backend-functions #'dumb-jump-xref-activate)
	(delq 'elpy--xref-backend xref-backend-functions))))
  ;; :config
  ;; (setq elpy-mode-map (make-sparse-keymap)
  ;; 	elpy-shell-map (make-sparse-keymap))
  ;; :bind (nil ;; Disable all elpy bindings
  ;; 	 :map elpy-mode-map
  ;; 	 ([t] . nil)
  ;; 	 :map elpy-shell-map
  ;; 	 ([t] . nil))
  ;; :bind (nil
  ;; 	 :map elpy-mode-map
  ;; 	 ;; Conflict with smart-scan
  ;; 	 ;; FIXME: Should we keep a binding for move line or region up/down?
  ;; 	 ;; Such a binding should be global
  ;; 	 ;; For now I'm also rebinding smart-scan, let's see if it sticks
  ;; 	 ("<M-down>" . nil)
  ;; 	 ("<M-up>" . nil)
  ;; 	 ;; Those bindings are way too fast
  ;; 	 ("<C-down>" . nil)
  ;; 	 ("<C-up>" . nil)
  ;; 	 ;; Conflicts with simple eval
  ;; 	 ("C-c C-e" . nil))
  )

(use-package cython-mode
  :mode (("\\.pyx\\'"  . cython-mode)
         ("\\.spyx\\'" . cython-mode)
         ("\\.pxd\\'"  . cython-mode)
         ("\\.pxi\\'"  . cython-mode))
  :delight (cython-mode ".pyx")
  :bind (nil
	 :map cython-mode-map
	 ("C-c C-y" . tv/sage-shell-paste-and-filter)
	 ("C-c C-j" . tv/sage-shell-newline-and-indent)))


(use-package sage-shell-mode
  :delight (sage-shell:sage-mode ".sage")
  :hook ((sage-shell:sage-mode
	  . (lambda ()
	      (setq-local company-backends
			  '((company-capf
			     :with company-dabbrev
			     :with company-files)))
	      (setq-local elpy-mode-map nil))))
  :custom
  (sage-shell:set-ipython-version-on-startup nil)
  (sage-shell:check-ipython-version-on-startup nil)
  (sage-shell)
  (sage-shell:use-prompt-toolkit nil)
  (sage-shell:simple-prompt t)
  :bind (nil
	 :map sage-shell:sage-mode-map
	 ("C-c C-e" . tv/sage-shell-send)
	 ("C-c C-y" . tv/sage-shell-paste-and-filter)
	 ("C-c C-j" . tv/sage-shell-newline-and-indent)
	 ("C-c C-b" . sage-shell-edit:send-buffer)
	 ("C-M-i" . counsel-company)))


(with-eval-after-load 'sage-shell-mode
  (setq sage-shell:use-prompt-toolkit nil)
  (defun sage-shell:-start-sage-process-readline (cmd buffer)
    (let ((cmdlist (split-string cmd)))
      (apply 'make-comint-in-buffer "Sage" buffer
             (car cmdlist) nil (cons "--simple-prompt" (cdr cmdlist))))))


; Jupyter

(with-eval-after-load 'tv/gui
  (when (and tv/emacs-has-modules tv/load-jupyter)
    (use-package jupyter)))

;; Magma

(defvar tv/using-magma-dev-version nil)
(defvar tv/path-to-dev-magma (f-join user-emacs-directory "devel/magma-mode"))

(if tv/using-magma-dev-version
    (use-package magma-mode :straight nil :load-path tv/path-to-dev-magma)
  (use-package magma-mode))

(use-package magma-mode
  :straight ;(:type built-in)
  :mode (("\\.mgm\\'" . magma-mode)
	 ;; The following extension is sometimes also used for Mathematica files
	 ("\\.m\\'"   . magma-mode))
  :bind (nil
	 :map magma-mode-map
	 ("C-M-i" . magma-completion-at-point)
	 ("C-c RET" . magma-comint-send-now)
	 ("C-c C-c" . magma-eval-buffer))
  :config
  (progn
    (tv/load-elisp "tv-magma.el")
    (with-eval-after-load 'yasnippet
      (require 'magma-snippets))
    (defun tv/magma-mode-hooks nil
      ;; the test is necessary to avoid triggering for org babel blocks
      (unless (eq major-mode "org-mode") (auto-insert))
      (subword-mode)
      (toggle-truncate-lines 1))
    (add-hook 'magma-mode-hook 'tv/magma-mode-hooks)
    (dolist (map (list magma-mode-map
		       magma-comint-interactive-mode-map))
      (key-combo-define
       map "="
       '(tv/insert-coloneq tv/magma-insert-testeq "=")))
    (setq magma-use-electric-newline   t
	  magma-interactive-use-comint t
	  magma-interactive-use-load   t
	  magma-interactive-wait-between-inputs t
	  magma-interactive-method 'expr
	  magma-interactive-auto-save t
	  magma-interactive-prompt nil
	  magma-completion-auto-update nil ;; Work-around slowness
	  magma-interactive-skip-empty-lines t
	  magma-interactive-skip-comments t
	  magma-initial-file 'default
	  magma-file-header 'tv/magma-update-header-custom))) 

;; Mathematica

(use-package wolfram-mode)

;; Maple

;; Dependency, for now
;; Or is it? The default recipe points to an outdated version 
;; (use-package folding)

(use-package maplev
  :straight (maplev
	     :type git
	     :host github
	     :repo "ThibautVerron/maplev")
  :config
  (key-combo-define maplev-mode-map "=" '(tv/insert-coloneq "="))
  :bind (nil
	 :map maplev-mode-map
	 ("C-<backspace>" . nil)
	 ("C-;" . nil)))

;;;; Useful aliases

;; This is typically commands that are useful enough that I do not
;; want to look for them every time I use them, but not useful enough
;; to warrant a shortcut. `M-x' followed by a few keys for the alias
;; and `RET' is short enough and easy to remember.

(defalias 'rb 'revert-buffer)



;; Custom settings
(load custom-file t)

;; Personal settings, end
(load tv/personal-init-file t)

;; Computer-local settings
(load tv/local-init-file t)
(load "~/.emacs-local.el" t)

;; If the OS is attempting to restart the applications as they were it
;; does not necessarily restart emacs as daemon so we do it now too.
(require 'server)
(unless (server-running-p) (server-start))

(put 'narrow-to-region 'disabled nil)
