(defcustom tv/personal-init-file
  (f-join user-emacs-directory "personal-post.el")
  "Path to a file with late elisp to evaluate."
  :group 'tv/custom)
(defcustom tv/personal-pre-init-file
  (f-join user-emacs-directory "personal-pre.el")
  "Path to a file with early elisp to evaluate."
  :group 'tv/custom)
(defcustom tv/local-init-file
  (f-join tv/user-local-emacs-directory "init.el")
  "Path to a file with local elisp to evaluate."
  :group 'tv/custom)
(defcustom tv/local-pre-init-file
  (f-join tv/user-local-emacs-directory "init-pre.el")
  "Path to a file with early local elisp to evaluate."
  :group 'tv/custom)

(defun tv/find-init-file (&optional new-frame)
  "Opens the main init file
With prefix argument, open it in a new frame."
  (interactive "P")
  (tv/find-file user-init-file new-frame))

(defun tv/find-personal-init-file (&optional new-frame)
  "Opens an init file with personal settings
With prefix argument, open it in a new frame."
  (interactive "P")
  (tv/find-file tv/personal-init-file new-frame))

(defun tv/find-personal-pre-init-file (&optional new-frame)
  "Opens an init file with early personal settings
With prefix argument, open it in a new frame."
  (interactive "P")
  (tv/find-file tv/personal-pre-init-file new-frame))

(defun tv/find-local-init-file (&optional new-frame)
  "Opens a local init file
With prefix argument, open it in a new frame."
  (interactive "P")
  (tv/find-file tv/local-init-file new-frame))

(defun tv/find-local-pre-init-file (&optional new-frame)
  "Opens a local early init file
With prefix argument, open it in a new frame."
  (interactive "P")
  (tv/find-file tv/local-pre-init-file new-frame))

(defalias 'init-main #'tv/find-init-file)
(defalias 'init #'tv/find-personal-init-file)
(defalias 'init-early #'tv/find-personal-pre-init-file)
(defalias 'init-local #'tv/find-local-init-file)
(defalias 'init-local-early #'tv/find-local-pre-init-file)
