;;; General

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(setq LaTeX-biblatex-use-Biber nil)

(setq TeX-electric-sub-and-superscript t)

(setq LaTeX-indent-environment-check t)
;; (setq LaTeX-indent-environment-list (-concat '(("tikzpicture" latex-indent) ("scope" latex-indent))
;; 					     LaTeX-indent-environment-list))

(setq LaTeX-indent-level 2)
(setq LaTeX-item-indent 0)
(setq LaTeX-left-right-indent-level 1)
(setq LaTeX-math-list (quote (("o" "circ" "" nil))))
(setq LaTeX-math-menu-unicode t)

(setq TeX-newline-function #'newline-and-indent)


(setq TeX-save-query nil)

; Should be linux specific
(setq TeX-source-correlate-method (quote synctex))
(setq TeX-source-correlate-mode t)
(setq TeX-source-correlate-start-server t)
(setq TeX-view-program-list (quote (("Open" "xdg-open %o"))))
(setq TeX-view-program-selection (quote ((output-pdf "Open") ((output-dvi style-pstricks) "dvips and gv") (output-dvi "xdvi") (output-html "xdg-open"))))

(bind-key "C-c C-z" #'tv/latex-command-block LaTeX-mode-map)

(setq TeX-PDF-mode t)

(provide 'tex-buf) ; work-around while waiting for an update, see https://github.com/tom-tan/auctex-latexmk/issues/44
(use-package auctex-latexmk
  :ensure
  ;:init 
  :config
  (progn
    (auctex-latexmk-setup)
    (setq auctex-latexmk-extra-options "") ; or "-pv"
    (setq-default TeX-command-default "LatexMk")))

(setq TeX-command-force nil) ; or ""

;; Preview

(set-default 'preview-scale-function 1.25)
(setq preview-auto-cache-preamble t)

;;; Flyspell

(autoload 'flyspell-babel-setup "flyspell-babel")
(setq flyspell-babel-command-alist
      '(("english" "english")
        ("french" "french")))

;;; Smartparens

(with-eval-after-load 'smartparens
  (let ((modes '(tex-mode plain-tex-mode latex-mode LaTeX-mode)))
    (dolist (open '("\\left(" "\\left[" "\\left\\{" "\\left|"
		    "\\bigl(" "\\biggl(" "\\Bigl(" "\\Biggl(" "\\bigl["
		    "\\biggl[" "\\Bigl[" "\\Biggl[" "\\bigl\\{" "\\biggl\\{"
		    "\\Bigl\\{" "\\Biggl\\{"
		    "\\lfloor" "\\lceil" "\\langle"
                      "\\lVert" "\\lvert" "`" "\""))
      (sp-local-pair modes open nil
		     :when '(:rem sp-in-math-p);; :actions :rem
		     ))
    ;; (sp-local-pair "\"" nil :actions :rem)
    ))

;; Smartparens does it better than AUCTeX
(bind-key "$" #'self-insert-command LaTeX-mode-map)

;; The default binding breaks with delete-selection-mode (and possibly
;; smartparens)
(bind-key "\\" #'self-insert-command LaTeX-mode-map)
;; FIXME: This should be the right thing to do but smartparens breaks it
;;(bind-key "\"" #'self-insert-command LaTeX-mode-map)
(bind-key "\"" #'TeX-insert-quote LaTeX-mode-map)

;; Maybe not necessary
(bind-key "C-c s (" #'tv/latex-insert-leftright-paren LaTeX-mode-map)
(bind-key "C-c s p" #'tv/latex-insert-leftright-paren LaTeX-mode-map)
(bind-key "C-c s {" #'tv/latex-insert-leftright-brace LaTeX-mode-map)
(bind-key "C-c s s" #'tv/latex-insert-leftright-brace LaTeX-mode-map)
(bind-key "C-c s [" #'tv/latex-insert-leftright-square LaTeX-mode-map)
(bind-key "C-c s q" #'tv/latex-insert-leftright-square LaTeX-mode-map)
(bind-key "C-c s <" #'tv/latex-insert-leftright-angle LaTeX-mode-map)
(bind-key "C-c s a" #'tv/latex-insert-leftright-angle LaTeX-mode-map)

;; (sp-with-modes '(
;;                  tex-mode
;;                  plain-tex-mode
;;                  latex-mode
;;                  LaTeX-mode
;;                  )
;;   (sp-local-pair "\\left(" "\\right."
;;                  :post-handlers '(sp-latex-insert-spaces-inside-pair))
;;   (sp-local-pair "\\left[" "\\right."
;;                  :post-handlers '(sp-latex-insert-spaces-inside-pair)))

;; cdlatex and co

(use-package cdlatex
  :hook (LaTeX-mode . turn-on-cdlatex)
  :custom
  (cdlatex-math-modify-prefix [f7]) ; as good as disabled?
  (cdlatex-make-sub-superscript-roman-if-pressed-twice t)
  :bind (nil
	 :map cdlatex-mode-map
	 ;; Let smartparens handle parentheses
	 ("(" . nil)
	 ("<" . nil)
	 ("[" . nil)
	 ("$" . nil)
	 ("{" . nil)
	 ("TAB" . nil)))

(bind-key "^" #'tv/cdlatex-sub-superscript LaTeX-mode-map)
(bind-key "_" #'tv/cdlatex-sub-superscript LaTeX-mode-map)

;; LaTeX-auto-activating-snippets
(setq laas-frac-snippet nil) ;; disable that

(defun tv/laas-not-in-word ()
  (not
   (and
    (member aas-transient-snippet-key laas-basic-snippets)
    (looking-back "\\w\\|\\\\" (- (point) 1)))))

(use-package laas
  :hook (LaTeX-mode . laas-mode)
  :hook (LaTeX-mode . (lambda ()
			(add-hook 'aas-global-condition-hook
				  #'tv/laas-not-in-word
				  nil 'local))))

;;; Reftex

(setq reftex-bibliography-commands '("bibliography" "nobibliography" "addbibresource"))
(setq reftex-plug-into-AUCTeX t)

(setq reftex-ref-macro-prompt nil)

;; FIXME: Make more robust (or buffer-local)
(defun tv/init-reftex-labels (symbol prop-envs)
  (setq reftex-label-alist
        (quote (("definition" 112 "def:" "~\\ref{%s}" nil
                 ("definition" "définition"))
                ("algorithm" 97 "algo:" "~\\ref{%s}" caption ("algorithm"))
	        ("\\item" 105 "item:" "~\\ref{%s}" item nil)
                ("\\subfloat[]{}" 116 nil "~\\ref{%s}" 1 nil))))
  (-each
      prop-envs
    '(lambda (env)
       (!cons (list env 112 "prop:" "#~\\ref{%s}" nil (list env)) reftex-label-alist)))
  (ignore-errors (reftex-compile-variables)))

(defcustom tv/reftex-prop-like-environments
  nil
  "List of environments to be treated like propositions by reftex.

This includes having a prop: label, and being accessible in the list of labels with the prefix p."
  :group 'tv-elisp
  :type 'list
  :set 'tv/init-reftex-labels
  )


(setq reftex-section-levels '(("part" . 0)
			      ("chapter" . 1)
			      ("section" . 2)
			      ("subsection" . 3)
			      ("subsubsection" . 4)
			      ("paragraph" . 5)
			      ("subparagraph" . 6)
			      ("frametitle" . 7)
			      ;; ("begin{frame}" . 7)
			      ("addchap" . -1)
			      ("addsec" . -2)))

;; Better completion than the reftex default, slightly nicer interface
(use-package ivy-bibtex
  :config (setq ivy-re-builders-alist
		'((ivy-bibtex . ivy--regex-ignore-order)
		  (t . ivy--regex-plus))
		ivy-bibtex-default-action 'ivy-bibtex-insert-citation)
  :bind (nil
	 :map reftex-mode-map
	  ("C-c [" . tv/ivy-bibtex-insert-citation)))

;; Edition of bibtex

(use-package bibtex
  :bind (nil
	 :map bibtex-mode-map
	 ("C-c C-e C-e" . doi-insert-bibtex)))

;; Beginend

;; (beginend-define-mode LaTeX-mode
;;   (progn
;;     (beginning-of-buffer)
;;     (re-search-forward "\\\\begin{document}" nil t)
;;     (forward-line 1))
;;   (progn
;;     (re-search-forward "\\\\end{document}" nil t)
;;     (forward-line -1)))


;; Improve styles

(TeX-add-style-hook
 "tikz"
 (lambda ()
   ;; "path" is defined by "url" with incompatible syntax
   (setq LaTeX-verbatim-macros-with-delims-local
   	 (remove "path"  LaTeX-verbatim-macros-with-delims-local))
   (setq LaTeX-verbatim-macros-with-braces-local
	 (remove "path"  LaTeX-verbatim-macros-with-braces-local))
   (when (and (featurep 'font-latex)
	      (eq TeX-install-font-lock 'font-latex-setup))
     (font-latex-add-keywords '(("node" "")
				("path" "")
				("draw" "")
				("coordinate" ""))
			      'function)
     (font-lock-flush))
   (LaTeX-add-environments
    '("scope"))))

(TeX-add-style-hook
 "pgfplots"
 (lambda ()
   (TeX-run-style-hooks "tikz")))

;; (TeX-load-style-file "tikz.el")


;; (TeX-add-style-hook
;;  "beamer"
;;  (lambda ()
;;    (when (and (featurep 'font-latex)
;; 	      (eq TeX-install-font-lock 'font-latex-setup))
;;      (font-latex-add-keywords '(("structure" "<{")
;; 				("alert" "<{"))
;; 			      'italic-command)
;;      (font-latex-add-keywords '(("alt" "<")
;; 				("visible" "<")
;; 				("only" "<")
;; 				("uncovered" "<"))
;; 			      'function))))
;; (TeX-load-style-file "beamer.el")

;;; Hooks

(defun tv/latex-hook-function ()
  ;; (flyspell-mode t)
  ;; (flyspell-babel-setup)
  (turn-on-reftex)
  (reftex-isearch-minor-mode t)
  (LaTeX-math-mode t)
  )

(add-hook 'LaTeX-mode-hook #'tv/latex-hook-function)



;;; Keys


(bind-key "C-c 1" #'TeX-next-error LaTeX-mode-map)
(bind-key "C-c `" #'tv/TeX-error-overview LaTeX-mode-map)
(bind-key "q" #'tv/TeX-error-overview-quit TeX-error-overview-mode-map)

