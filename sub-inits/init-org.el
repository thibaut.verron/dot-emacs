;;;; General settings

(setq
 org-pretty-entities t ; pretty printing of characters
 org-pretty-entities-include-sub-superscripts nil
 org-use-sub-superscripts '{} ; 
 org-adapt-indentation nil
)

(use-package org-superstar
  :hook
  (org-mode . org-superstar-mode)
  :custom
  (org-superstar-leading-bullet ?\s))

;;;; General note taking

;; TODO: investigate org-roam?

(require 'org-refile)
(setq org-default-notes-file (f-join org-directory "notes.org"))

(bind-key "C-c c" #'org-capture)

;;;; Org agenda


;;;; Task management

(setq
 org-log-done t ; Log when tasks change status
 org-log-into-drawer t
 org-use-fast-todo-selection 'expert)

(setq
 org-todo-keywords
 '((sequence "TODO(t!)" "STARTED(s!)" "WAITING(w!)" "|" "DONE(d!)" "CANCELLED(c)" )))

(setq org-todo-keyword-faces
      '(("TODO" . "red")
	("STARTED" . "orange")
        ("WAITING" . "orange")
        ("CANCEL" . "grey")
	("DONE" . "green")))

;; Math inputs

(use-package cdlatex
  :init (add-hook 'org-mode-hook #'turn-on-org-cdlatex)
  :bind (nil
	 :map org-cdlatex-mode-map
	 ("$" . cdlatex-dollar)))

;;;; Tables

(setq
 org-table-tab-jumps-over-hlines t)

;;;; Capture

(setq org-directory "~/org/")

(setq org-agenda-files (list org-directory))

;; Do not overwrite it!
(bind-key "C-c [" 'ignore org-mode-map)
(bind-key "C-c ]" 'ignore org-mode-map)
(bind-key "C-c a" 'org-agenda)

(defconst tv/org-inbox-file (concat org-directory "inbox.org"))

(require 'org-protocol)

(setq org-capture-templates
      `(("t" "todo" entry (file ,tv/org-inbox-file)
         "* TODO %?")
	("i" "inbox" entry (file ,tv/org-inbox-file)
         "* %?")
	("a" "agenda" entry (file ,tv/org-inbox-file)
         "* %?")
	;; for org-protocol
	("L" "link" entry (file ,tv/org-inbox-file)
         "* [[%:link][%:description]]" :immediate-finish t)
        ("p" "org-protocol-capture" entry (file ,tv/org-inbox-file)
         "* [[%:link][%:description]]\n\n %i" :immediate-finish t)))
        

;;;; Org babel

(use-package ob-python
  :straight (:type built-in))

;; Experimental
(use-package ob-magma
  :after magma-mode
  :straight (ob-magma
	     :type git
	     :repo "ThibautVerron/ob-magma"
	     :host github))

(use-package ob-latex
  :straight (:type built-in))

(setq
 org-src-strip-leading-and-trailing-blank-lines t
 org-src-fontify-natively t)

;;;; Similar bindings to auctex

(bind-keys
 :map org-mode-map
 ("C-c C-j" . org-meta-return)
 )

;;;; Org export

(setq org-export-async-init-file (concat user-emacs-directory "init-async.el"))


