(defconst tv/home-directory (getenv "HOME"))
(defconst tv/user-local-emacs-directory
  (expand-file-name ".emacs-local.d/" tv/home-directory))

(defconst tv/system-type (replace-regexp-in-string "/" "-" (format "%s" system-type)))
(defconst tv/system-string (concat tv/system-type "-" emacs-version))

;; Straight
(setq straight-use-package-by-default t  
      straight-recipes-gnu-elpa-use-mirror t
      straight-build-dir (expand-file-name
			  (concat "build" "-" tv/system-string)
			  tv/user-local-emacs-directory)
      straight-repository-branch "develop")

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el"
	       user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Load use-package
(straight-use-package 'use-package)
(require 'use-package)

;; Settings

(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment 'utf-8)

;; Libraries

(use-package f)

;; Org

(straight-use-package
 '(org-plus-contrib
   :repo "https://code.orgmode.org/bzg/org-mode.git"
   ))

(use-package org)

(load (concat user-emacs-directory "personal-async.el") t)


;; Silence those "mark set"

(defun push-mark (&optional location nomsg activate)
  "Set mark at LOCATION (point, by default) and push old mark on mark ring.
If the last global mark pushed was not in the current buffer,
also push LOCATION on the global mark ring.
Display `Mark set' unless the optional second arg NOMSG is non-nil.

Novice Emacs Lisp programmers often try to use the mark for the wrong
purposes.  See the documentation of `set-mark' for more information.

In Transient Mark mode, activate mark if optional third arg ACTIVATE non-nil."
  (when (mark t)
    (let ((old (nth mark-ring-max mark-ring))
          (history-delete-duplicates nil))
      (add-to-history 'mark-ring (copy-marker (mark-marker)) mark-ring-max t)
      (when old
        (set-marker old nil))))
  (set-marker (mark-marker) (or location (point)) (current-buffer))
  ;; Don't push the mark on the global mark ring if the last global
  ;; mark pushed was in this same buffer.
  (unless (and global-mark-ring
               (eq (marker-buffer (car global-mark-ring)) (current-buffer)))
    (let ((old (nth global-mark-ring-max global-mark-ring))
          (history-delete-duplicates nil))
      (add-to-history
       'global-mark-ring (copy-marker (mark-marker)) global-mark-ring-max t)
      (when old
        (set-marker old nil))))
  ;; (or nomsg executing-kbd-macro (> (minibuffer-depth) 0)
  ;;     (message "Mark set"))
  (if (or activate (not transient-mark-mode))
      (set-mark (mark t)))
  nil)

;; Debug

;;(message "%s" (org-link-get-parameter "doi" :export))
